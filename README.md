# 云手机（一代）平台功能介绍

![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/184756_d6124eb0_1437763.png "屏幕截图.png")

#### 项目介绍
- 云手机管理平台，基于dubbo进行远程服务调用，并通过虚拟化技术kvm, xen, vmware, virtualbox等，建立androidx86操作系统，对虚拟机进行统计管理，批量增加和销毁模拟器；

- 通过实现RFB协议实现对vncserver的控制，可以用于desktop控制器，编写统一的脚本控制；

只作为技术交流 切勿商业用途

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2018/0912/171035_6d1214b9_1437763.png "QQ截图20180912170945.png")


#### 环境要求

1. zookeeper
2. dubbo
3. jdk1.8
4. tomcat8
5. mysql
6. redis

#### 运行界面
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/151353_75fa28bf_1437763.png)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/151412_430c52cf_1437763.png)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/151428_7fa2233b_1437763.png)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/151439_6ee9048c_1437763.png)
![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/174801_b440705d_1437763.png)

siged 鲲鹏云工作室