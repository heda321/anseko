package org.i9.slb.platform.anseko.android;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private static final String TAG = "i9-android-plugin-kit";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    /**
     * startBtn点击事件
     *
     * @param v
     */
    public void startBtn(View v) {
        Log.d(TAG, "点击启动事件");
    }

    /**
     * stopBtn点击事件
     *
     * @param v
     */
    public void stopBtn(View v) {
        Log.d(TAG, "点击停止事件");
    }
}