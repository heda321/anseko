package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataInput;
import java.io.IOException;

public class Colour {

    public int r;
    public int g;
    public int b;

    public Colour() {
    }

    public void readData(DataInput datainput)
            throws IOException {
        r = datainput.readUnsignedShort();
        g = datainput.readUnsignedShort();
        b = datainput.readUnsignedShort();
    }
}
