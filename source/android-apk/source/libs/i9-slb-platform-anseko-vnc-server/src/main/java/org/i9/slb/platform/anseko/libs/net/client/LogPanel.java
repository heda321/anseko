package org.i9.slb.platform.anseko.libs.net.client;

public interface LogPanel {

    public static final int INFO = 0;
    public static final int IS = 1;
    public static final int OS = 2;
    public static final int START = 8;
    public static final int EXIT = 9;

    public abstract void showMsg(String s, String s1, int i);
}
