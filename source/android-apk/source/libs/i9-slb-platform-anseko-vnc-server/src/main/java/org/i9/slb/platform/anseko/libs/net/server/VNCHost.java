package org.i9.slb.platform.anseko.libs.net.server;

import org.i9.slb.platform.anseko.libs.net.logging.VLogger;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBAuthenticator;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBServer;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBSocket;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class VNCHost implements Runnable {

    private int display;
    private String displayName;
    private RFBAuthenticator authenticator;
    private Constructor constructor;
    private RFBServer sharedServer = null;
    private boolean isRunning;
    private boolean threadFinished;
    private ServerSocket serverSocket;
    private ArrayList servers = new ArrayList();
    private boolean reverseMode = false;
    private String mappid;

    public VNCHost(int paramInt, String paramString, Class paramClass, RFBAuthenticator paramRFBAuthenticator,
                   boolean paramBoolean, String mappid)
            throws NoSuchMethodException {
        this.reverseMode = paramBoolean;
        this.mappid = mappid;
        this.constructor = paramClass.getDeclaredConstructor(new Class[]{Integer.TYPE, String.class});
        if (!(RFBServer.class.isAssignableFrom(paramClass)))
            throw new NoSuchMethodException("Class does not support RFBServer interface");
        this.display = paramInt;
        this.displayName = paramString;
        this.authenticator = paramRFBAuthenticator;
        new Thread(this).start();
    }

    public synchronized void setSharedServer(RFBServer paramRFBServer) {
        this.sharedServer = paramRFBServer;
    }

    public synchronized RFBServer getSharedServer() {
        return this.sharedServer;
    }

    private void openServer() {
        try {
            this.serverSocket = new ServerSocket(5999);
        } catch (Exception localException1) {
            System.out.println("MSG4" + this.displayName + ":" + (5900 + this.display));
            System.out.println("MSG5" + localException1.getMessage());
            localException1.printStackTrace();
            close();
            return;
        }
        VLogger.getLogger().log("MSG6", this.displayName + ":" + (5900 + this.display));
        try {
            setSharedServer((RFBServer) this.constructor.newInstance(new Object[]{new Integer(this.display), this.displayName}));
            while (this.isRunning) {
                Socket localSocket = this.serverSocket.accept();
                if (this.servers.size() == 0) {
                    InetAddress localInetAddress = localSocket.getInetAddress();
                    System.out.println("MSG7-" + localInetAddress.getHostAddress() + " - " + localInetAddress.getCanonicalHostName());
                    RFBSocket localRFBSocket = new RFBSocket(localSocket, getSharedServer(), this, this.authenticator, this.mappid);
                    this.servers.add(localRFBSocket);
                }
//        try
//        {
//          localSocket.close();
//        }
//        catch (Exception localException3)
//        {
//        }
            }
        } catch (Exception localException2) {
            if (this.isRunning)
                System.out.println("MSG5-" + localException2.getMessage());
        } finally {
            this.threadFinished = true;
            System.out.println("MSG8-" + this.displayName);
            close();
        }
    }

    private void addClientSocket() {
        System.out.println("begin to execute:addClientSocket ");
        try {
            Socket localSocket = new Socket(this.displayName, 5999);
            localSocket.isConnected();
            if (this.servers.size() == 0) {
                setSharedServer((RFBServer) this.constructor.newInstance(new Object[]{new Integer(this.display), this.displayName}));
                InetAddress localInetAddress = localSocket.getInetAddress();
                System.out.println("MSG7" + localInetAddress.getHostAddress() + " - " + localInetAddress.getCanonicalHostName());
                RFBSocket localRFBSocket = new RFBSocket(localSocket, getSharedServer(), this, this.authenticator, this.mappid, false);
                this.servers.add(localRFBSocket);

            } else {
                try {
                    localSocket.close();
                } catch (Exception localException2) {
                }
            }
            while ((this.isRunning) && (localSocket.isConnected()))
                try {
                    Thread.sleep(100L);
                } catch (Exception localException3) {
                    localException3.printStackTrace();
                }
            System.out.println("Stopped Client mode at " + this.displayName);
            try {
                localSocket.close();
            } catch (Exception localException4) {
            }
        } catch (UnknownHostException localUnknownHostException) {
            if (this.isRunning)
                System.out.println("MSG4" + localUnknownHostException.getMessage());
        } catch (IOException localIOException) {
            if (this.isRunning)
                System.out.println("MSG4" + localIOException.getMessage());
        } catch (SecurityException localSecurityException) {
            if (this.isRunning)
                System.out.println("MSG4" + localSecurityException.getMessage());
        } catch (Exception localException1) {
            if (this.isRunning)
                System.out.println("MSG5" + localException1.getMessage());
        } finally {
            this.threadFinished = true;
            System.out.println("MSG8" + this.displayName);
            close();
        }
    }

    public void run() {
        this.isRunning = true;
        this.threadFinished = false;
        if (this.reverseMode)
            addClientSocket();
        else
            openServer();
    }

    public void close(RFBSocket paramRFBSocket) {
        try {
            System.out.println("Close:" + paramRFBSocket);
            this.servers.remove(paramRFBSocket);
            System.out.println("Removed?:" + this.servers.size());
        } catch (Exception localException) {
        }
    }

    public void close() {
        if (!(this.isRunning))
            return;
        try {
            this.isRunning = false;
            try {
                this.serverSocket.close();
            } catch (Exception localException1) {
            }
            this.serverSocket = null;
            while (!(this.threadFinished))
                try {
                    Thread.currentThread();
                    Thread.sleep(20L);
                } catch (InterruptedException localInterruptedException) {
                }
            while (this.servers.size() > 0) {
                RFBSocket localRFBSocket = (RFBSocket) this.servers.get(0);
                localRFBSocket.close();
                try {
                    this.servers.remove(localRFBSocket);
                } catch (Exception localException3) {
                }
            }
            if (this.sharedServer != null)
                this.sharedServer.close();
        } catch (Exception localException2) {
            localException2.printStackTrace();
        } finally {
            this.serverSocket = null;
        }
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
