package org.i9.slb.platform.anseko.libs.net.rfb;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.DataOutput;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			Raw, RRE, CoRREStub, Hextile, 
//			PixelFormat

public abstract class Rect
        implements Cloneable {

    public int x;
    public int y;
    public int w;
    public int h;
    public int count;

    public static int bestEncoding(int ai[]) {
        int i = 0;
        do {
            if (i >= ai.length)
                break;
            switch (ai[i]) {
                case 0: // '\0'
                case 5: // '\005'
                    return ai[i];
            }
            i++;
        } while (true);
        return 5;
    }

    public static Rect encode(int i, PixelFormat pixelformat, Image image, int j, int k, int l, int i1) {
        int ai[] = new int[l * i1];
        PixelGrabber pixelgrabber = new PixelGrabber(image, j, k, l, i1, ai, 0, l);
        try {
            pixelgrabber.grabPixels();
        } catch (InterruptedException interruptedexception) {
        }
        return encode(i, ai, pixelformat, j, k, l, j, k, l, i1);
    }

    public static Rect encode(int i, PixelFormat pixelformat, BufferedImage bufferedimage, int j, int k) {
        int l = bufferedimage.getWidth();
        int i1 = bufferedimage.getHeight();
        int ai[] = new int[l * i1];
        PixelGrabber pixelgrabber = new PixelGrabber(bufferedimage, 0, 0, l, i1, ai, 0, l);
        try {
            pixelgrabber.grabPixels();
        } catch (InterruptedException interruptedexception) {
        }
        return encode(i, ai, pixelformat, j, k, l, j, k, l, i1);
    }

    public static Rect encode(int i, int ai[], PixelFormat pixelformat, int j, int k, int l, int i1, int j1) {
        return encode(i, ai, pixelformat, 0, 0, j, k, l, i1, j1);
    }

    public static Rect encode(int i, int ai[], PixelFormat pixelformat, int j, int k, int l, int i1, int j1,
                              int k1, int l1) {
        if (k1 == 0 && l1 == 0) {
            Exception exception = new Exception("w==h==0");
            exception.printStackTrace();
        }
        switch (i) {
            case 0: // '\0'
                return new Raw(ai, pixelformat, j, k, l, i1, j1, k1, l1);

            case 1: // '\001'
                return null;

            case 2: // '\002'
                return new RRE(ai, pixelformat, j, k, l, i1, j1, k1, l1);

            case 4: // '\004'
                return new CoRREStub(ai, pixelformat, j, k, l, i1, j1, k1, l1);

            case 5: // '\005'
                return new Hextile(ai, pixelformat, j, k, l, i1, j1, k1, l1);

            case 3: // '\003'
            default:
                return null;
        }
    }

    public Rect(int i, int j, int k, int l) {
        count = 1;
        x = i;
        y = j;
        w = k;
        h = l;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        dataoutput.writeShort(x);
        dataoutput.writeShort(y);
        dataoutput.writeShort(w);
        dataoutput.writeShort(h);
    }

    public void transform(int i, int j) {
        x += i;
        y += j;
    }

    public String toString() {
        return String.valueOf(x) + "," + y + "," + w + "," + h;
    }

    public Object clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Rect not cloneable");
    }

    protected static int[] copyPixels(int ai[], int i, int j, int k, int l, int i1) {
        int j1 = l * i1;
        int ai1[] = new int[j1];
        int k1 = i - l;
        int l1 = 0;
        int i2 = k * i + j;
        for (int j2 = 0; j2 < j1; ) {
            if (l1 == l) {
                l1 = 0;
                i2 += k1;
            }
            ai1[j2] = ai[i2];
            j2++;
            l1++;
            i2++;
        }

        return ai1;
    }

    protected static void writePixel(DataOutput dataoutput, PixelFormat pixelformat, int i)
            throws IOException {
        i = pixelformat.translatePixel(i);
        switch (pixelformat.bitsPerPixel) {
            case 32: // ' '
                dataoutput.writeByte(i & 0xff);
                dataoutput.writeByte(i >> 8 & 0xff);
                dataoutput.writeByte(i >> 16 & 0xff);
                dataoutput.writeByte(i >> 24 & 0xff);
                break;

            case 16: // '\020'
                dataoutput.writeByte(i & 0xff);
                dataoutput.writeByte(i >> 8 & 0xff);
                break;

            case 8: // '\b'
                dataoutput.writeByte(i & 0xff);
                break;
        }
    }

    protected static int getBackground(int ai[], int i, int j, int k, int l, int i1) {
        return ai[k * i + j];
    }
}
