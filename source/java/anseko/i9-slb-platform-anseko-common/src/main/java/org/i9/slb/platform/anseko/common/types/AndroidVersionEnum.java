package org.i9.slb.platform.anseko.common.types;

import java.util.HashMap;

public enum AndroidVersionEnum {

    ANDROID_VERSION_5(5, "android-x86.v5"), ANDROID_VERSION_6(6, "android-x86.v6"),

    ANDROID_VERSION_8(8, "android-x86.v8");

    private final int index;

    private final String name;

    AndroidVersionEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    private static final HashMap<Integer, AndroidVersionEnum> types = new HashMap<Integer, AndroidVersionEnum>();

    static {
        for (AndroidVersionEnum androidVersionEnum : AndroidVersionEnum.values()) {
            types.put(androidVersionEnum.getIndex(), androidVersionEnum);
        }
    }

    public static AndroidVersionEnum valueOf(int index) {
        return types.get(index);
    }

}
