package org.i9.slb.platform.anseko.common.types;

/**
 * 模拟器运行状态
 *
 * @author R12
 * @date 2018年9月6日 16:26:45
 */
public enum PowerStateEnum {

    /**
     * 关机
     * “
     */
    CLOSE("关机"),

    /**
     * 运行中
     */
    RUNING("运行中");

    PowerStateEnum(String name) {
        this.name = name;
    }

    final String name;

    public String getName() {
        return name;
    }

    public static PowerStateEnum valueOf(int value) {
        for (PowerStateEnum powerState : values()) {
            if (powerState.ordinal() == value) {
                return powerState;
            }
        }
        return PowerStateEnum.RUNING;
    }
}
