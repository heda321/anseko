package org.i9.slb.platform.anseko.common.types;

/**
 * 虚拟化类型
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:32
 */
public enum VirtualEnum {

    KVM(1), XEN(2), VMWARE(3), VIRTUALBOX(4);

    VirtualEnum(int value) {
        this.value = value;
    }

    private final int value;

    public int getValue() {
        return value;
    }

    public static VirtualEnum valueOf(int value) {
        for (VirtualEnum virtualEnum : values()) {
            if (virtualEnum.getValue() == value) {
                return virtualEnum;
            }
        }
        return VirtualEnum.KVM;
    }
}
