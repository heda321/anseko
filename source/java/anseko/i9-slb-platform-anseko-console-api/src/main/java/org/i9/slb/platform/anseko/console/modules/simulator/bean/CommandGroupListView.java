package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import java.util.List;

/**
 * 命令执行组
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 11:45
 */
public class CommandGroupListView implements java.io.Serializable {

    private static final long serialVersionUID = -3129515414420324940L;

    private List<CommandGroupInfoView> list;

    private int totalRow;

    public List<CommandGroupInfoView> getList() {
        return list;
    }

    public void setList(List<CommandGroupInfoView> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
