package org.i9.slb.platform.anseko.console.modules.simulator.service.impl;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.*;
import org.i9.slb.platform.anseko.console.modules.simulator.service.CommandService;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 下行指令service
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 13:35
 */
@Service("commandServiceImpl")
public class CommandServiceImpl implements CommandService {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    /**
     * 获取模拟器下所有的命令组列表
     *
     * @param commandGroupSearch
     * @return
     */
    @Override
    public CommandGroupListView getCommandGroupListPage(CommandGroupSearch commandGroupSearch) {
        CommandGroupSearchDto commandGroupSearchDto = new CommandGroupSearchDto();
        commandGroupSearchDto.setSimulatorId(commandGroupSearch.getSimulatorId());
        commandGroupSearchDto.setStartPage(commandGroupSearch.getStartPage());
        commandGroupSearchDto.setPageSize(commandGroupSearch.getPageSize());

        DubboResult<CommandGroupListDto> dubboResult = this.dubboCommandRemoteService.getCommandGroupDtoListPage(commandGroupSearchDto);
        dubboResult.tryBusinessException();

        CommandGroupListDto commandGroupListDto = dubboResult.getRe();
        List<CommandGroupInfoView> list = new ArrayList<CommandGroupInfoView>();
        for (CommandGroupDto commandGroupDto : commandGroupListDto.getList()) {
            CommandGroupInfoView commandGroupInfoView = new CommandGroupInfoView();
            commandGroupInfoView.copyProperty(commandGroupDto);
            list.add(commandGroupInfoView);
        }

        CommandGroupListView commandGroupListView = new CommandGroupListView();
        commandGroupListView.setList(list);
        commandGroupListView.setTotalRow(commandGroupListDto.getTotalRow());
        return commandGroupListView;
    }

    /**
     * 获取模拟器下所有的命令执行列表
     *
     * @param commandExecuteSearch
     * @return
     */
    @Override
    public CommandExecuteListView getCommandExecuteListPage(CommandExecuteSearch commandExecuteSearch) {
        CommandExecuteSearchDto commandExecuteSearchDto = new CommandExecuteSearchDto();
        commandExecuteSearchDto.setCommandGroupId(commandExecuteSearch.getCommandGroupId());
        commandExecuteSearchDto.setStartPage(commandExecuteSearch.getStartPage());
        commandExecuteSearchDto.setPageSize(commandExecuteSearch.getPageSize());
        DubboResult<CommandExecuteListDto> dubboResult = this.dubboCommandRemoteService.getCommandExecuteListPage(commandExecuteSearchDto);
        dubboResult.tryBusinessException();

        List<CommandExecuteInfoView> list = new ArrayList<>();
        CommandExecuteListDto commandExecuteListDto = dubboResult.getRe();
        for (CommandExecuteDto commandExecuteDto : commandExecuteListDto.getList()) {
            CommandExecuteInfoView commandExecuteInfoView = new CommandExecuteInfoView();
            commandExecuteInfoView.copyProperty(commandExecuteDto);
            list.add(commandExecuteInfoView);
        }

        CommandExecuteListView commandExecuteListView = new CommandExecuteListView();
        commandExecuteListView.setList(list);
        commandExecuteListView.setTotalRow(commandExecuteListDto.getTotalRow());
        return commandExecuteListView;
    }

    /**
     * 获取命令执行详情
     *
     * @param commandId
     * @return
     */
    @Override
    public CommandExecuteInfoView getCommandExecuteInfo(String commandId) {
        DubboResult<CommandExecuteDto> dubboResult = this.dubboCommandRemoteService.getCommandExecuteDto(commandId);
        dubboResult.tryBusinessException();

        CommandExecuteDto commandExecuteDto = dubboResult.getRe();
        CommandExecuteInfoView commandExecuteInfoView = new CommandExecuteInfoView();
        commandExecuteInfoView.copyProperty(commandExecuteDto);
        return commandExecuteInfoView;
    }

    /**
     * 获取命令组详情
     *
     * @param commandGroupId
     * @return
     */
    @Override
    public CommandGroupInfoView getCommandGroupInfo(String commandGroupId) {
        DubboResult<CommandGroupDto> dubboResult = this.dubboCommandRemoteService.getCommandGroupDto(commandGroupId);
        dubboResult.tryBusinessException();

        CommandGroupDto commandGroupDto = dubboResult.getRe();
        CommandGroupInfoView commandGroupInfoView = new CommandGroupInfoView();
        commandGroupInfoView.copyProperty(commandGroupDto);
        return commandGroupInfoView;
    }

    @Override
    public List<CommandExecuteInfoView> getCommandExecuteList(String commandGroupId) {
        List<CommandExecuteInfoView> list = new ArrayList<CommandExecuteInfoView>();

        DubboResult<List<CommandExecuteDto>> dubboResult = this.dubboCommandRemoteService.getCommandExecuteDtosCommandGroupId(commandGroupId);
        dubboResult.tryBusinessException();

        for (CommandExecuteDto commandExecuteDto : dubboResult.getRe()) {
            CommandExecuteInfoView commandExecuteInfoView = new CommandExecuteInfoView();
            commandExecuteInfoView.copyProperty(commandExecuteDto);
            list.add(commandExecuteInfoView);
        }
        return list;
    }
}
