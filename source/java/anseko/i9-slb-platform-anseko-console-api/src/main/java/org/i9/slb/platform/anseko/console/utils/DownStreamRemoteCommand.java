package org.i9.slb.platform.anseko.console.utils;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DownStreamRemoteCommand implements IDownStreamRemoteCommand {

    @Autowired
    private CoreserviceRemoteService coreserviceRemoteService;

    @Autowired
    private CommandExecuteRemoteService commandExecuteRemoteService;

    /**
     * 执行shell命令操作
     *
     * @param simulatorId
     * @param nameLabel
     * @param shellCommandParamDto
     */
    @Override
    public void shellCommandExecute(String simulatorId, String nameLabel, ShellCommandParamDto shellCommandParamDto) {
        InstanceDto instanceDto = this.coreserviceRemoteService.remoteServiceGetInstanceDtoInfo(simulatorId);
        this.coreserviceRemoteService.remoteServiceLaunchCommandDispatch(simulatorId, nameLabel, shellCommandParamDto);
        commandExecuteRemoteService.shellCommandExecute(instanceDto.getId(), shellCommandParamDto);
    }

    /**
     * 执行多个shell命令操作
     *
     * @param simulatorId
     * @param nameLabel
     * @param shellCommandParamDtos
     */
    @Override
    public void shellCommandExecuteBatch(String simulatorId, String nameLabel, List<ShellCommandParamDto> shellCommandParamDtos) {
        if (CollectionUtils.isEmpty(shellCommandParamDtos)) {
            return;
        }
        InstanceDto instanceDto = this.coreserviceRemoteService.remoteServiceGetInstanceDtoInfo(simulatorId);
        this.coreserviceRemoteService.remoteServiceLaunchCommandDispatch(simulatorId, nameLabel, shellCommandParamDtos);
        this.commandExecuteRemoteService.shellCommandExecuteBatch(instanceDto.getId(), shellCommandParamDtos);
    }

    /**
     * 执行命令组（混合命令）
     *
     * @param simulatorId
     * @param nameLabel
     * @param groupCommandParamDto
     */
    @Override
    public void multipleCommandExecute(String simulatorId, String nameLabel, GroupCommandParamDto groupCommandParamDto) {
        InstanceDto instanceDto = this.coreserviceRemoteService.remoteServiceGetInstanceDtoInfo(simulatorId);
        this.coreserviceRemoteService.remoteServiceLaunchCommandDispatch(simulatorId, nameLabel, groupCommandParamDto);
        this.commandExecuteRemoteService.multipleCommandExecute(instanceDto.getId(), groupCommandParamDto);
    }
}
