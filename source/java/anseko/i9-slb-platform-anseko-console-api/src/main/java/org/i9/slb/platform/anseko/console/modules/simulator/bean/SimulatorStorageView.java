package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.provider.dto.SimulatorStorageDto;

import java.util.Date;

public class SimulatorStorageView implements java.io.Serializable {

    private static final long serialVersionUID = 4069711347233915785L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 存储名称
     */
    private String storageName;
    /**
     * 路径
     */
    private String storagePath;
    /**
     * 容量
     */
    private Integer storageVolume;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 更新日期
     */
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public Integer getStorageVolume() {
        return storageVolume;
    }

    public void setStorageVolume(Integer storageVolume) {
        this.storageVolume = storageVolume;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void copyProperty(SimulatorStorageDto simulatorStorageDto) {
        this.id = simulatorStorageDto.getId();
        this.simulatorId = simulatorStorageDto.getSimulatorId();
        this.createDate = simulatorStorageDto.getCreateDate();
        this.updateDate = simulatorStorageDto.getUpdateDate();
        this.storageName = simulatorStorageDto.getStorageName();
        this.storagePath = simulatorStorageDto.getStoragePath();
        this.storageVolume = simulatorStorageDto.getStorageVolume();
    }
}
