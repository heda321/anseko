package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.downstream.IFileCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.IMultipleCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.IShellCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 指定实例执行远程调用服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 15:33
 */
@Service("commandExecuteRemoteService")
public class CommandExecuteRemoteService {

    private IShellCommandExecuteRemoteService shellCommandExecuteRemoteService;

    private IFileCommandExecuteRemoteService fileCommandExecuteRemoteService;

    private IMultipleCommandExecuteRemoteService multipleCommandExecuteRemoteService;

    public void shellCommandExecute(String clientId, ShellCommandParamDto shellCommandParamDto) {
        this.shellCommandExecuteRemoteService.shellCommandExecute(clientId, shellCommandParamDto);
    }

    public void shellCommandExecuteBatch(String clientId, List<ShellCommandParamDto> shellCommandParamDtos) {
        this.shellCommandExecuteRemoteService.shellCommandExecuteBatch(clientId, shellCommandParamDtos);
    }

    public void fileCommandExecute(String clientId, FileCommandParamDto fileCommandParamDto) {
        fileCommandExecuteRemoteService.fileCommandExecute(clientId, fileCommandParamDto);
    }

    public void fileCommandExecuteBatch(String clientId, List<FileCommandParamDto> fileCommandParamDtos) {
        this.fileCommandExecuteRemoteService.fileCommandExecuteBatch(clientId, fileCommandParamDtos);
    }

    public void multipleCommandExecute(String clientId, GroupCommandParamDto groupCommandParamDto) {
        this.multipleCommandExecuteRemoteService.multipleCommandExecute(clientId, groupCommandParamDto);
    }
}
