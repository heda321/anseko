package org.i9.slb.platform.anseko.console.modules.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.console.modules.user.service.UserTokenRedisService;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service("redisServiceImpl")
public class UserTokenRedisServiceImpl implements UserTokenRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 刷新用户token过期时间
     *
     * @param userToken
     */
    @Override
    public void refreshUserTokenExpire(String userToken) {
        this.stringRedisTemplate.expire(userToken, 30 * 60, TimeUnit.SECONDS);
    }

    /**
     * 获取用户token值
     *
     * @param userToken
     * @return
     */
    @Override
    public String getUserTokenValue(String userToken) {
        return this.stringRedisTemplate.opsForValue().get(userToken);
    }

    /**
     * 保存用户token值
     *
     * @param userToken
     * @param value
     */
    @Override
    public void saveUserTokenToRedisDb(String userToken, String value) {
        this.stringRedisTemplate.opsForValue().set(userToken, value, 30 * 60, TimeUnit.SECONDS);
    }

    /**
     * 创建用户token值，并保存在redis中
     *
     * @param userDto
     */
    @Override
    public String createUserTokenToRedisDb(UserDto userDto) {
        String userToken = UUID.randomUUID().toString();
        this.saveUserTokenToRedisDb(userToken, JSONObject.toJSONString(userDto));
        return userToken;
    }

    @Override
    public void deleteUserToken(String userToken) {
        this.stringRedisTemplate.delete(userToken);
    }
}
