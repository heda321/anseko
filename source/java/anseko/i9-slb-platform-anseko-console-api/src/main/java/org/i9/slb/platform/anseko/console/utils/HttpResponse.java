package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.exception.BusinessException;

/**
 * http响应数据封装
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 14:46
 */
public class HttpResponse {

    public static <T> HttpResult<T> ok() {
        HttpResult httpResult = new HttpResult();
        httpResult.setResult(ErrorCode.SUCCESS);
        httpResult.setMessage("操作成功");
        return httpResult;
    }

    public static <T> HttpResult<T> ok(T re) {
        HttpResult<T> httpResult = new HttpResult<T>();
        httpResult.setResult(ErrorCode.SUCCESS);
        httpResult.setMessage("操作成功");
        httpResult.setRe(re);
        return httpResult;
    }

    public static <T> HttpResult<T> error(BusinessException e) {
        HttpResult<T> httpResult = new HttpResult<T>();
        httpResult.setResult(ErrorCode.BUSINESS_EXCEPTION);
        httpResult.setMessage(e.getMessage());
        return httpResult;
    }

    public static <T> HttpResult<T> error(int result, BusinessException e) {
        HttpResult<T> httpResult = new HttpResult<T>();
        httpResult.setResult(result);
        httpResult.setMessage(e.getMessage());
        return httpResult;
    }

    public static <T> HttpResult<T> error(Exception e) {
        HttpResult<T> httpResult = new HttpResult<T>();
        if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            httpResult.setResult(ErrorCode.BUSINESS_EXCEPTION);
            httpResult.setMessage(businessException.getMessage());
        } else {
            httpResult.setResult(ErrorCode.UNKOWN_ERROR);
            httpResult.setMessage("未知错误");
        }
        return httpResult;
    }
}
