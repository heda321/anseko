package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.provider.dto.SimulatorDisplayDto;

import java.util.Date;

public class SimulatorDisplayView implements java.io.Serializable {

    private static final long serialVersionUID = 7728197998250246296L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 密码
     */
    private String password;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 更新日期
     */
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void copyProperty(SimulatorDisplayDto simulatorDisplayDto) {
        this.id = simulatorDisplayDto.getId();
        this.simulatorId = simulatorDisplayDto.getSimulatorId();
        this.createDate = simulatorDisplayDto.getCreateDate();
        this.updateDate = simulatorDisplayDto.getUpdateDate();
        this.port = simulatorDisplayDto.getPort();
        this.password = simulatorDisplayDto.getPassword();
    }
}
