package org.i9.slb.platform.anseko.provider;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.types.PowerStateEnum;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class DubboSimulatorRemoteServiceTest extends JunitBaseTest {

    @Autowired
    private IDubboSimulatorRemoteService dubboSimulatorRemoteService;

    @Test
    public void testGetSimulatorDtos() {
        DubboResult<List<SimulatorDto>> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtos();
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        for (SimulatorDto simulatorDto : dubboResult.getRe()) {
            System.out.println(JSONObject.toJSONString(simulatorDto));
        }
    }

    @Test
    public void testGetSimulatorDto() {
        String simulatorId = "063c117a-c291-47e5-9300-435381bed47c";
        DubboResult<SimulatorDto> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDto(simulatorId);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        System.out.println(JSONObject.toJSONString(dubboResult.getRe()));
    }

    @Test
    public void testRemoveSimulatorInfo() {
        String simulatorId = "063c117a-c291-47e5-9300-435381bed47c";
        DubboResult<?> dubboResult = this.dubboSimulatorRemoteService.removeSimulatorInfo(simulatorId);
        System.out.println(JSONObject.toJSONString(dubboResult));
    }

    @Test
    public void testRefreshSimulatorPowerState() {
        String simulatorId = "0ba85a57-f7ef-412f-9536-399d70275114";
        DubboResult<?> dubboResult = this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorId, PowerStateEnum.RUNING.ordinal());
        System.out.println(JSONObject.toJSONString(dubboResult));
    }

    @Test
    public void testCreateSimulatorInfo() {
        for (int i = 0; i < 50; i++) {
            SimulatorDto simulatorDto = new SimulatorDto();
            simulatorDto.setId(UUID.randomUUID().toString());
            simulatorDto.setCpunum(4);
            simulatorDto.setRamnum(4096);
            simulatorDto.setPowerStatus(PowerStateEnum.CLOSE.ordinal());
            simulatorDto.setAndroidVersion(5);
            simulatorDto.setSimulatorName(UUIDUtil.generateUUID());
            simulatorDto.setInstanceId("69509a7b-58b3-454d-a57c-2dec9aabbcdc");
            DubboResult<?> dubboResult = dubboSimulatorRemoteService.createSimulatorInfo(simulatorDto);
            System.out.println(JSONObject.toJSONString(dubboResult));
        }
    }

    @Test
    public void testGetAndroidSimulatorDto() {
        String simulatorId = "063c117a-c291-47e5-9300-435381bed47c";
        DubboResult<SimulatorDto> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDto(simulatorId);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(dubboResult);
            return;
        }
        System.out.println(JSONObject.toJSONString(dubboResult.getRe()));
    }
}
