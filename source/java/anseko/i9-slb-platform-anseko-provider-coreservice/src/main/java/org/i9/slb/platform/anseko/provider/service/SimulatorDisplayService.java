package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDisplayDto;
import org.i9.slb.platform.anseko.provider.entity.SimulatorDisplayEntity;
import org.i9.slb.platform.anseko.provider.repository.SimulatorDisplayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 模拟器显示器服务类
 *
 * @author r12
 * @date 2019年3月7日 09:51:02
 */
@Service
public class SimulatorDisplayService {

    private static final Integer VNC_PORT_DEFAULT_VALUE = 5901;

    public Integer getNextVNCPort(String instanceId) {
        Integer port = this.simulatorDisplayRepository.queryNextVNCPort();
        if (port == null) {
            return VNC_PORT_DEFAULT_VALUE;
        }
        return port + 1;
    }

    @Autowired
    private SimulatorDisplayRepository simulatorDisplayRepository;

    /**
     * 创建模拟器显示器
     *
     * @param simulatorId
     * @param simulatorDisplayDto
     */
    @Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
    public void createSimulatorDisplay(String simulatorId, SimulatorDisplayDto simulatorDisplayDto) {
        SimulatorDisplayEntity simulatorDisplayEntity = new SimulatorDisplayEntity();
        simulatorDisplayEntity.setId(UUIDUtil.generateUUID());
        simulatorDisplayEntity.setSimulatorId(simulatorId);
        simulatorDisplayEntity.setPort(simulatorDisplayDto.getPort());
        simulatorDisplayEntity.setPassword(simulatorDisplayDto.getPassword());
        simulatorDisplayEntity.setCreateDate(new Date());
        simulatorDisplayEntity.setUpdateDate(new Date());
        this.simulatorDisplayRepository.insertSimulatorDisplay(simulatorDisplayEntity);
    }

    /**
     * 获取模拟器显示器
     *
     * @param simulatorId
     * @return
     */
    public SimulatorDisplayEntity getSimulatorDisplayEntity(String simulatorId) {
        SimulatorDisplayEntity simulatorDisplayEntity = this.simulatorDisplayRepository.getSimulatorDisplayEntity(simulatorId);
        return simulatorDisplayEntity;
    }

    /**
     * 复制属性
     *
     * @param simulatorDisplayEntity
     * @param simulatorDisplayDto
     */
    public void copyProperty(SimulatorDisplayEntity simulatorDisplayEntity, SimulatorDisplayDto simulatorDisplayDto) {
        simulatorDisplayDto.setId(simulatorDisplayEntity.getId());
        simulatorDisplayDto.setSimulatorId(simulatorDisplayEntity.getSimulatorId());
        simulatorDisplayDto.setCreateDate(simulatorDisplayEntity.getCreateDate());
        simulatorDisplayDto.setUpdateDate(simulatorDisplayEntity.getUpdateDate());
        simulatorDisplayDto.setPort(simulatorDisplayEntity.getPort());
        simulatorDisplayDto.setPassword(simulatorDisplayEntity.getPassword());
    }
}
