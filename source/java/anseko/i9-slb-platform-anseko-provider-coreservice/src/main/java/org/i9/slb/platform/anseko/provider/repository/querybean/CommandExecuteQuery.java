package org.i9.slb.platform.anseko.provider.repository.querybean;

/**
 * 命令执行查询分页
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/2/27 14:13
 */
public class CommandExecuteQuery extends BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = 1053570351616726417L;

    private String commandGroupId;

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }
}
