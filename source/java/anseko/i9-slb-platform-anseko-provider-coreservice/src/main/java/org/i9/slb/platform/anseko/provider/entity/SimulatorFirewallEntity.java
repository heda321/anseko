package org.i9.slb.platform.anseko.provider.entity;

import java.util.Date;

public class SimulatorFirewallEntity implements java.io.Serializable {

    private static final long serialVersionUID = 4153988156306433560L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 显示标签
     */
    private String nameLabel;
    /**
     * 源端口
     */
    private Integer sport;
    /**
     * 目标端口
     */
    private Integer dport;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 更新日期
     */
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(String nameLabel) {
        this.nameLabel = nameLabel;
    }

    public Integer getSport() {
        return sport;
    }

    public void setSport(Integer sport) {
        this.sport = sport;
    }

    public Integer getDport() {
        return dport;
    }

    public void setDport(Integer dport) {
        this.dport = dport;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
