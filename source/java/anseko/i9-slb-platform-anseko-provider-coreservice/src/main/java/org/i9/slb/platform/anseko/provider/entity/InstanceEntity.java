package org.i9.slb.platform.anseko.provider.entity;

/**
 * 实例实体类
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:34
 */
public class InstanceEntity implements java.io.Serializable {

    private static final long serialVersionUID = 2829891044758459940L;
    /**
     * 编号
     */
    private String id;
    /**
     * 实例名称
     */
    private String instanceName;
    /**
     * 虚拟化类型
     *
     * @see org.i9.slb.platform.anseko.common.types.VirtualEnum
     */
    private Integer virtualType;
    /**
     * 远程地址
     */
    private String remoteAddress;
    /**
     * 创建日期
     */
    private String createDate;
    /**
     * 更新日期
     */
    private String updateDate;
    /**
     * 状态
     *
     * @see org.i9.slb.platform.anseko.common.types.InstanceStatusEnum
     */
    private Integer status;

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public Integer getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(Integer virtualType) {
        this.virtualType = virtualType;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
