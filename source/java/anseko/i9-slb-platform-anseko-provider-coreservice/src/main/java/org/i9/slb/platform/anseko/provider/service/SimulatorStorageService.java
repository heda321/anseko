package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.SimulatorStorageDto;
import org.i9.slb.platform.anseko.provider.entity.SimulatorStorageEntity;
import org.i9.slb.platform.anseko.provider.repository.SimulatorStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 模拟器存储服务类
 *
 * @author r12
 * @date 2019年3月7日 11:00:26
 */
@Service
public class SimulatorStorageService {

    @Autowired
    private SimulatorStorageRepository simulatorStorageRepository;

    /**
     * 创建模拟器存储
     *
     * @param simulatorId
     * @param simulatorStorageDtos
     */
    public void createSimulatorStorage(String simulatorId, List<SimulatorStorageDto> simulatorStorageDtos) {
        for (SimulatorStorageDto simulatorStorageDto : simulatorStorageDtos) {
            SimulatorStorageEntity simulatorStorageEntity = new SimulatorStorageEntity();
            simulatorStorageEntity.setId(UUIDUtil.generateUUID());
            simulatorStorageEntity.setSimulatorId(simulatorId);
            simulatorStorageEntity.setStorageName(simulatorStorageDto.getStorageName());
            simulatorStorageEntity.setStoragePath(simulatorStorageDto.getStoragePath());
            simulatorStorageEntity.setStorageVolume(simulatorStorageDto.getStorageVolume());
            simulatorStorageEntity.setCreateDate(new Date());
            simulatorStorageEntity.setUpdateDate(new Date());
            this.simulatorStorageRepository.insertSimulatorStorage(simulatorStorageEntity);
        }
    }

    /**
     * 获取模拟器存储
     *
     * @param simulatorId
     * @return
     */
    public List<SimulatorStorageEntity> getSimulatorStorageEntityList(String simulatorId) {
        List<SimulatorStorageEntity> list = this.simulatorStorageRepository.getSimulatorStorageEntityList(simulatorId);
        return list;
    }

    /**
     * 复制属性
     *
     * @param simulatorStorageEntity
     * @param simulatorStorageDto
     */
    public void copyProperty(SimulatorStorageEntity simulatorStorageEntity, SimulatorStorageDto simulatorStorageDto) {
        simulatorStorageDto.setId(simulatorStorageEntity.getId());
        simulatorStorageDto.setSimulatorId(simulatorStorageEntity.getSimulatorId());
        simulatorStorageDto.setCreateDate(simulatorStorageEntity.getCreateDate());
        simulatorStorageDto.setUpdateDate(simulatorStorageEntity.getUpdateDate());
        simulatorStorageDto.setStorageName(simulatorStorageEntity.getStorageName());
        simulatorStorageDto.setStoragePath(simulatorStorageEntity.getStoragePath());
        simulatorStorageDto.setStorageVolume(simulatorStorageEntity.getStorageVolume());
    }
}
