package org.i9.slb.platform.anseko.provider.service.dubbo;

import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.types.InstanceStatusEnum;
import org.i9.slb.platform.anseko.common.utils.DateUtil;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceListDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceSearchDto;
import org.i9.slb.platform.anseko.provider.entity.InstanceEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.InstanceQuery;
import org.i9.slb.platform.anseko.provider.service.InstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 实例远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:41
 */
@Service("dubboInstanceRemoteService")
public class DubboInstanceRemoteService implements IDubboInstanceRemoteService {

    @Autowired
    private InstanceService instanceService;

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    @Override
    public DubboResult<InstanceDto> getInstanceDto(String id) {
        DubboResult<InstanceDto> dubboResult = new DubboResult<InstanceDto>();
        try {
            InstanceEntity instanceEntity = this.instanceService.getInstanceEntityById(id);
            InstanceDto instanceDto = new InstanceDto();
            this.copyProperty(instanceEntity, instanceDto);
            dubboResult.setRe(instanceDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;

    }

    /**
     * 获取实例列表
     *
     * @return
     */
    @Override
    public DubboResult<List<InstanceDto>> getInstanceDtos() {
        List<InstanceDto> list = new ArrayList<InstanceDto>();
        for (InstanceEntity instanceEntity : this.instanceService.getInstanceEntityList()) {
            InstanceDto instanceDto = new InstanceDto();
            this.copyProperty(instanceEntity, instanceDto);
            list.add(instanceDto);
        }
        DubboResult<List<InstanceDto>> dubboResult = new DubboResult<List<InstanceDto>>();
        dubboResult.setRe(list);
        return dubboResult;
    }

    /**
     * 创建实例
     *
     * @param instanceDto
     */
    @Override
    public DubboResult<?> createInstanceInfo(InstanceDto instanceDto) {
        try {
            this.instanceService.checkInstanceNameIsAlreadyExist(instanceDto.getInstanceName());
            this.instanceService.checkRemoteAddressIsAlreadyExist(instanceDto.getRemoteAddress());
            InstanceEntity instanceEntity = new InstanceEntity();
            instanceEntity.setId(instanceDto.getId());
            instanceEntity.setInstanceName(instanceDto.getInstanceName());
            instanceEntity.setRemoteAddress(instanceDto.getRemoteAddress());
            instanceEntity.setVirtualType(instanceDto.getVirtualType());
            instanceEntity.setCreateDate(DateUtil.formatNow());
            instanceEntity.setUpdateDate(DateUtil.formatNow());
            instanceEntity.setStatus(InstanceStatusEnum.ENABLE.getIndex());
            this.instanceService.insertInstanceEntity(instanceEntity);
            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        }
    }

    /**
     * 更新实例信息
     *
     * @param instanceDto
     */
    @Override
    public DubboResult<?> updateInstanceInfo(InstanceDto instanceDto) {
        InstanceEntity instanceEntity = new InstanceEntity();
        instanceEntity.setId(instanceDto.getId());
        instanceEntity.setInstanceName(instanceDto.getInstanceName());
        instanceEntity.setVirtualType(instanceDto.getVirtualType());
        instanceEntity.setRemoteAddress(instanceDto.getRemoteAddress());
        instanceEntity.setUpdateDate(DateUtil.formatNow());
        this.instanceService.updateInstanceEntity(instanceEntity);
        return DubboResult.ok();
    }

    /**
     * 获取实例列表分页
     *
     * @param instanceSearchDto
     * @return
     */
    @Override
    public DubboResult<InstanceListDto> getInstanceDtosPage(InstanceSearchDto instanceSearchDto) {
        InstanceQuery instanceQuery = new InstanceQuery();
        instanceQuery.setStartPage(instanceSearchDto.getStartPage());
        instanceQuery.setPageSize(instanceSearchDto.getPageSize());
        instanceQuery.setInstanceName(instanceSearchDto.getInstanceName());

        PageInfo<InstanceEntity> pageInfo = this.instanceService.getInstanceEntityListPage(instanceQuery);

        List<InstanceDto> list = new ArrayList<InstanceDto>();
        for (InstanceEntity instanceEntity : pageInfo.getList()) {
            InstanceDto instanceDto = new InstanceDto();
            this.copyProperty(instanceEntity, instanceDto);
            list.add(instanceDto);
        }

        InstanceListDto instanceListDto = new InstanceListDto();
        instanceListDto.setList(list);
        instanceListDto.setTotalRow((int) pageInfo.getTotal());

        DubboResult<InstanceListDto> dubboResult = new DubboResult<InstanceListDto>();
        dubboResult.setRe(instanceListDto);
        return dubboResult;
    }

    @Override
    public DubboResult<?> updateInstanceStatus(String instanceId, Integer status) {
        InstanceEntity instanceEntity = new InstanceEntity();
        instanceEntity.setId(instanceId);
        instanceEntity.setStatus(status);
        this.instanceService.updateInstanceEntity(instanceEntity);
        return DubboResult.ok();
    }

    @Override
    public DubboResult<List<InstanceDto>> getIstanceDtosOnline() {
        List<InstanceDto> list = new ArrayList<InstanceDto>();
        for (InstanceEntity instanceEntity : this.instanceService.getInstanceEntityListOnline()) {
            InstanceDto instanceDto = new InstanceDto();
            this.copyProperty(instanceEntity, instanceDto);
            list.add(instanceDto);
        }
        return DubboResult.ok(list);
    }

    private void copyProperty(InstanceEntity instanceEntity, InstanceDto instanceDto) {
        instanceDto.setId(instanceEntity.getId());
        instanceDto.setInstanceName(instanceEntity.getInstanceName());
        instanceDto.setRemoteAddress(instanceEntity.getRemoteAddress());
        instanceDto.setVirtualType(instanceEntity.getVirtualType());
        instanceDto.setCreateDate(instanceEntity.getCreateDate());
        instanceDto.setUpdateDate(instanceEntity.getUpdateDate());
        instanceDto.setStatus(instanceEntity.getStatus());
    }
}
