package org.i9.slb.platform.anseko.provider.service.dubbo;

import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.utils.DateUtil;
import org.i9.slb.platform.anseko.common.utils.Md5Util;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.IDubboUserRemoteService;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.i9.slb.platform.anseko.provider.dto.UserListDto;
import org.i9.slb.platform.anseko.provider.dto.UserPasswordDto;
import org.i9.slb.platform.anseko.provider.dto.UserSearchDto;
import org.i9.slb.platform.anseko.provider.entity.UserEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.UserQuery;
import org.i9.slb.platform.anseko.provider.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/12 13:45
 */
@Service("dubboUserRemoteService")
public class DubboUserRemoteService implements IDubboUserRemoteService {

    @Autowired
    private UserService userService;

    /**
     * 通过用户名获取用户信息
     *
     * @param username
     * @return
     */
    @Override
    public DubboResult<UserDto> getUserDtoByUsername(String username) {
        UserEntity userEntity = this.userService.getUserEntityByUsername(username);
        if (userEntity == null) {
            DubboResult<UserDto> dubboResult = new DubboResult<UserDto>();
            dubboResult.setResult(ErrorCode.BUSINESS_EXCEPTION);
            dubboResult.setMessage("用户不存在");
            return dubboResult;
        }

        UserDto userDto = new UserDto();
        this.copyProperty(userEntity, userDto);

        DubboResult<UserDto> dubboResult = new DubboResult<UserDto>();
        dubboResult.setRe(userDto);
        return dubboResult;
    }

    /**
     * 检查用户名是否已存在
     *
     * @param username
     * @return
     */
    @Override
    public DubboResult<?> checkUsernameAlreadyExist(String username) {
        int count = this.userService.countUserEntityByUsernameNumber(username);
        if (count == 0) {
            return DubboResult.ok();
        } else {
            return DubboResult.error(new BusinessException("用户名已存在"));
        }
    }

    /**
     * 通过编号获取用户信息
     *
     * @param id
     * @return
     */
    @Override
    public DubboResult<UserDto> getUserDtoById(String id) {
        DubboResult<UserDto> dubboResult = new DubboResult<UserDto>();
        try {
            UserEntity userEntity = this.userService.getUserEntityById(id);
            UserDto userDto = new UserDto();
            this.copyProperty(userEntity, userDto);
            dubboResult.setRe(userDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;
    }

    /**
     * 获取用户列表
     *
     * @param userSearchDto
     * @return
     */
    @Override
    public DubboResult<UserListDto> getUserDtos(UserSearchDto userSearchDto) {
        UserQuery userQuery = new UserQuery();
        userQuery.setStartPage(userSearchDto.getStartPage());
        userQuery.setPageSize(userSearchDto.getPageSize());
        userQuery.setUsername(userSearchDto.getUsername());

        List<UserDto> list = new ArrayList<UserDto>();

        PageInfo<UserEntity> pageInfo = this.userService.getUserEntityList(userQuery);
        for (UserEntity userEntity : pageInfo.getList()) {
            UserDto userDto = new UserDto();
            this.copyProperty(userEntity, userDto);
            list.add(userDto);
        }

        UserListDto userListDto = new UserListDto();
        userListDto.setList(list);
        userListDto.setTotalRow((int) pageInfo.getTotal());

        DubboResult<UserListDto> dubboResult = new DubboResult<UserListDto>();
        dubboResult.setRe(userListDto);
        return dubboResult;
    }

    /**
     * 创建用户
     *
     * @param userDto
     */
    @Override
    public DubboResult<?> createUserInfo(UserDto userDto) {
        try {
            final String username = userDto.getUsername();
            int count = this.userService.countUserEntityByUsernameNumber(username);
            if (count != 0) {
                throw new BusinessException("用户名已存在");
            }

            UserEntity userEntity = new UserEntity();
            userEntity.setId(UUIDUtil.generateUUID());
            userEntity.setUsername(userDto.getUsername());
            userEntity.setPassword(Md5Util.MD5(userDto.getPassword()));
            userEntity.setNickname(userDto.getNickname());
            userEntity.setPortraitUrl(userDto.getPortraitUrl());
            userEntity.setCreateDate(DateUtil.formatNow());
            userEntity.setUpdateDate(DateUtil.formatNow());
            this.userService.insertUserEntity(userEntity);
            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        }
    }

    /**
     * 更新用户
     *
     * @param userDto
     */
    @Override
    public DubboResult<?> updateUserInfo(UserDto userDto) {
        try {
            UserEntity userEntity = new UserEntity();
            userEntity.setNickname(userDto.getNickname());
            userEntity.setPortraitUrl(userDto.getPortraitUrl());
            userEntity.setUpdateDate(DateUtil.formatNow());
            userEntity.setId(userDto.getId());
            this.userService.updateUserEntity(userEntity);
            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        }
    }

    /**
     * 删除用户信息
     *
     * @param id
     */
    @Override
    public DubboResult<?> deleteUserById(String id) {
        this.userService.deleteUserById(id);
        return DubboResult.ok();
    }

    /**
     * 更新用户密码
     *
     * @param userPasswordDto
     * @return
     */
    @Override
    public DubboResult<?> changeUserPassword(UserPasswordDto userPasswordDto) {
        try {
            this.userService.updateUserEntityPassword(userPasswordDto.getUserId(), userPasswordDto.getPassword());
            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        }
    }

    public void copyProperty(UserEntity userEntity, UserDto userDto) {
        userDto.setId(userEntity.getId());
        userDto.setUsername(userEntity.getUsername());
        userDto.setPassword(userEntity.getPassword());
        userDto.setNickname(userEntity.getNickname());
        userDto.setPortraitUrl(userEntity.getPortraitUrl());
        userDto.setCreateDate(userEntity.getCreateDate());
        userDto.setUpdateDate(userEntity.getUpdateDate());
    }
}
