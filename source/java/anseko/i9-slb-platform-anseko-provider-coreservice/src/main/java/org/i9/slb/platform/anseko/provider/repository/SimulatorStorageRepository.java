package org.i9.slb.platform.anseko.provider.repository;

import org.i9.slb.platform.anseko.provider.entity.SimulatorStorageEntity;

import java.util.List;

public interface SimulatorStorageRepository {

    public void insertSimulatorStorage(SimulatorStorageEntity simulatorStorageEntity);

    List<SimulatorStorageEntity> getSimulatorStorageEntityList(String simulatorId);
}
