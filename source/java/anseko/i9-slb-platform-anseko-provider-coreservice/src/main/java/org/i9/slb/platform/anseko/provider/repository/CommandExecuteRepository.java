package org.i9.slb.platform.anseko.provider.repository;

import org.apache.ibatis.annotations.Param;
import org.i9.slb.platform.anseko.provider.entity.CommandExecuteEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandExecuteQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 命令执行dao
 *
 * @author R12
 * @date 2018年9月4日 10:50:31
 */
@Repository
public interface CommandExecuteRepository {

    /**
     * 保存命令执行信息
     *
     * @param commandExecuteEntity
     */
    void insertCommandExecuteEntity(CommandExecuteEntity commandExecuteEntity);

    /**
     * 更新命令执行状态
     *
     * @param commandId
     * @param commandResult
     */
    void updateCommandExecuteEntityCommandResult(@Param("commandId") String commandId, @Param("commandResult") String commandResult);

    /**
     * 获取命令调度执行列表
     *
     * @param commandGroupId
     * @return
     */
    List<CommandExecuteEntity> getCommandExecuteEntityListByGroupId(@Param("commandGroupId") String commandGroupId);

    /**
     * 获取命令执行信息
     *
     * @param commandId
     * @return
     */
    CommandExecuteEntity getCommandExecuteEntityListByCommandId(@Param("commandId") String commandId);

    /**
     * 获取命令执行列表分页
     *
     * @param commandExecuteQuery
     * @return
     */
    List<CommandExecuteEntity> getCommandExecuteEntityListPage(CommandExecuteQuery commandExecuteQuery);
}
