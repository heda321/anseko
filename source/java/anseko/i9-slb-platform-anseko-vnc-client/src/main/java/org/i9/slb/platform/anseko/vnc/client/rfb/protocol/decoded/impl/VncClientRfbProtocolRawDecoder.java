package org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl;

import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * vnc客户端rfb协议解码器（Raw）
 *
 * @author r12
 * @date 2019年2月1日 10:26:55
 */
public class VncClientRfbProtocolRawDecoder extends VncClientRfbProtocolDecoder {

    private VncClientMessageReader reader;

    public VncClientRfbProtocolRawDecoder(VncClientMessageReader reader) {
        this.reader = reader;
    }

    @Override
    public void readRect(int x, int y, int width, int height, VncClientMessageHandler handler) {
        byte[] imageBuf = reader.getImageBuf(width, width * height);
        int nPixels = imageBuf.length / (reader.bpp() / 8);
        int nRows = nPixels / width;
        int bytesPerRow = width * (reader.bpp() / 8);
        while (height > 0) {
            if (nRows > height) {
                nRows = height;
            }
            reader.getInputStream().readBytes(imageBuf, 0, nRows * bytesPerRow);
            handler.imageRect(x, y, width, nRows, imageBuf, 0);
            height -= nRows;
            y += nRows;
        }
    }

}
