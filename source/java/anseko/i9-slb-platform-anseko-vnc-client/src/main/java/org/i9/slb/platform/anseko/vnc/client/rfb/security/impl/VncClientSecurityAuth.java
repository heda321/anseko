package org.i9.slb.platform.anseko.vnc.client.rfb.security.impl;

import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.SecurityTypes;
import org.i9.slb.platform.anseko.vnc.client.rfb.security.VncClientSecurity;
import org.i9.slb.platform.anseko.vnc.client.utils.SecurityAuthUtil;

/**
 * vnc客户端安全认证（auth）
 *
 * @author r12
 * @date 2019年2月1日 10:16:58
 */
public class VncClientSecurityAuth extends VncClientSecurity {

    public VncClientSecurityAuth(String password) {
        this.password = password;
    }

    /**
     * 处理安全认证消息
     *
     * @param adapter
     * @return
     */
    @Override
    public boolean processMessage(VncClientMessageHandlerAdapter adapter) {
        RfbInputStream inputStream = adapter.getInputStream();
        RfbOutputStream outputStream = adapter.getOutputStream();
        byte[] challenge = new byte[SecurityAuthUtil.challengeSize];
        inputStream.readBytes(challenge, 0, SecurityAuthUtil.challengeSize);
        SecurityAuthUtil.encryptChallenge(challenge, this.password);
        outputStream.writeBytes(challenge, 0, SecurityAuthUtil.challengeSize);
        outputStream.flush();
        return true;
    }

    /**
     * 返回认证类型
     *
     * @return
     */
    @Override
    public int getType() {
        return SecurityTypes.vncAuth;
    }

    private String password;
}
