package org.i9.slb.platform.anseko.vnc.client.rfb.security.impl;

import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.SecurityTypes;
import org.i9.slb.platform.anseko.vnc.client.rfb.security.VncClientSecurity;

/**
 * vnc客户端安全认证（none）
 *
 * @author r12
 * @date 2019年2月1日 10:16:58
 */
public class VncClientSecurityNone extends VncClientSecurity {

    /**
     * 处理安全认证消息
     *
     * @param adapter
     * @return
     */
    @Override
    public boolean processMessage(VncClientMessageHandlerAdapter adapter) {
        return true;
    }

    /**
     * 返回认证类型
     *
     * @return
     */
    @Override
    public int getType() {
        return SecurityTypes.none;
    }
}
