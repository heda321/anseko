package org.i9.slb.platform.anseko.vnc.client.rfb.protocol;

import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;

public class PixelBuffer {

    public byte[] data;

    protected PixelFormat pixelFormat;

    protected int width_, height_;

    public PixelBuffer() {
        setPixelFormat(new PixelFormat());
    }

    public void setPixelFormat(PixelFormat pixelFormat) {
        if (pixelFormat.bpp != 8) {
            throw new RfbProtocolException("Internal setBusinessException: bpp must be 8 in PixelBuffer");
        }
        this.pixelFormat = pixelFormat;
    }

    public PixelFormat getPixelFormat() {
        return pixelFormat;
    }

    public final int width() {
        return width_;
    }

    public final int height() {
        return height_;
    }

    public final int area() {
        return width_ * height_;
    }

    public int getStride() {
        return width_;
    }

    public void fillRect(int x, int y, int w, int h, int pix) {
        int bytesPerPixel = getPixelFormat().bpp / 8;
        int bytesPerRow = bytesPerPixel * getStride();
        for (int ry = y; ry < y + h; ry++) {
            for (int rx = x; rx < x + w; rx++) {
                data[ry * bytesPerRow + rx] = (byte) pix;
            }
        }
    }

    public void imageRect(int x, int y, int w, int h, byte[] pix, int offset) {
        int bytesPerPixel = getPixelFormat().bpp / 8;
        int bytesPerDestRow = bytesPerPixel * getStride();
        for (int j = 0; j < h; j++)
            System.arraycopy(pix, offset + j * w, data, (y + j) * bytesPerDestRow + x, w);
    }

    public void copyRect(int x, int y, int w, int h, int srcX, int srcY) {
        int dest = x + y * getStride();
        int src = srcX + srcY * getStride();
        int inc = getStride();
        if (y > srcY) {
            src += (h - 1) * inc;
            dest += (h - 1) * inc;
            inc = -inc;
        }
        int destEnd = dest + h * inc;

        while (dest != destEnd) {
            System.arraycopy(data, src, data, dest, w);
            src += inc;
            dest += inc;
        }
    }

    public void maskRect(int x, int y, int w, int h, byte[] pix, byte[] mask) {
        int maskBytesPerRow = (w + 7) / 8;
        int stride = getStride();
        for (int j = 0; j < h; j++) {
            int cy = y + j;
            if (cy >= 0 && cy < height_) {
                for (int i = 0; i < w; i++) {
                    int cx = x + i;
                    if (cx >= 0 && cx < width_) {
                        int byte_ = j * maskBytesPerRow + i / 8;
                        int bit = 7 - i % 8;
                        if ((mask[byte_] & (1 << bit)) != 0) {
                            data[cy * stride + cx] = pix[j * w + i];
                        }
                    }
                }
            }
        }
    }
}
