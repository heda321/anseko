package org.i9.slb.platform.anseko.vnc.client.rfb.message.reader;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * rfb协议vnc客户端消息读取
 *
 * @author r12
 * @date 2019年2月1日 10:22:34
 */
abstract public class VncClientMessageReader {

    private static final Logger LOGGER = Logger.getLogger(VncClientMessageReader.class);

    protected VncClientMessageHandler handler;

    protected RfbInputStream inputStream;

    private VncClientRfbProtocolDecoder[] decoders;

    private byte[] imageBuf;

    private int imageBufSize;

    private int imageBufIdealSize;

    protected VncClientMessageReader(VncClientMessageHandler vncClientMessageHandler, RfbInputStream inputStream) {
        this.handler = vncClientMessageHandler;
        this.inputStream = inputStream;
        decoders = new VncClientRfbProtocolDecoder[Encodings.MAX + 1];
    }

    abstract public void readServerInit();

    abstract public void readMessage();

    public RfbInputStream getInputStream() {
        return inputStream;
    }

    public byte[] getImageBuf(int required, int requested) {
        int requiredBytes = required * (this.bpp() / 8);
        int requestedBytes = requested * (this.bpp() / 8);

        int size = requestedBytes;
        if (size > imageBufIdealSize) {
            size = imageBufIdealSize;
        }
        if (size < requiredBytes) {
            size = requiredBytes;
        }
        if (imageBufSize < size) {
            imageBufSize = size;
            imageBuf = new byte[imageBufSize];
        }
        return imageBuf;
    }

    public final int bpp() {
        return this.handler.getConnectionParameter().getPixelFormat().bpp;
    }

    protected void readSetColourMapEntries() {
        this.inputStream.skip(1);
        int firstColour = this.inputStream.readU16();
        int nColours = this.inputStream.readU16();
        int[] rgbs = new int[nColours * 3];
        for (int i = 0; i < nColours * 3; i++) {
            rgbs[i] = this.inputStream.readU16();
        }
        readEndMessage();
        this.handler.setColourMapEntries(firstColour, nColours, rgbs);
    }

    protected void readBell() {
        readEndMessage();
        this.handler.bell();
    }

    protected void readServerCutText() {
        this.inputStream.skip(3);
        int len = this.inputStream.readU32();
        if (len > 256 * 1024) {
            this.inputStream.skip(len);
            LOGGER.error("cut text too long (" + len + " bytes) - ignoring");
            return;
        }
        byte[] buf = new byte[len];
        this.inputStream.readBytes(buf, 0, len);
        readEndMessage();
        this.handler.serverCutText(new String(buf, 0, len));
    }

    protected void readEndMessage() {
    }

    protected void readFramebufferUpdateStart() {
        readEndMessage();
        this.handler.framebufferUpdateStart();
    }

    protected void readFramebufferUpdateEnd() {
        readEndMessage();
        this.handler.framebufferUpdateEnd();
    }

    protected void readRect(int x, int y, int w, int h, int encoding) {
        if ((x + w > this.handler.getConnectionParameter().getWidth()) || (y + h > this.handler.getConnectionParameter().getHeight())) {
            int width = this.handler.getConnectionParameter().getWidth();
            int height = this.handler.getConnectionParameter().getHeight();
            LOGGER.error("Rect too big: " + w + "x" + h + " at " + x + "," + y + " exceeds " + width + "x"
                    + height);
            throw new RfbProtocolException("Rect too big");
        }

        if (w * h == 0) {
            LOGGER.info("Ignoring zero size rect");
            return;
        }

        this.handler.beginRect(x, y, w, h, encoding);

        if (encoding == Encodings.COPY_RECT) {
            readCopyRect(x, y, w, h);
        } else {
            if (decoders[encoding] == null) {
                decoders[encoding] = VncClientRfbProtocolDecoder.createDecoder(encoding, this);
                if (decoders[encoding] == null) {
                    LOGGER.error("Unknown rect encoding " + encoding);
                    throw new RfbProtocolException("Unknown rect encoding");
                }
            }
            decoders[encoding].readRect(x, y, w, h, this.handler);
        }

        this.handler.endRect(x, y, w, h, encoding);
    }

    protected void readCopyRect(int x, int y, int w, int h) {
        int srcX = this.inputStream.readU16();
        int srcY = this.inputStream.readU16();
        this.handler.copyRect(x, y, w, h, srcX, srcY);
    }

    protected void readSetCursor(int hotspotX, int hotspotY, int w, int h) {
        int dataLen = w * h * (this.handler.getConnectionParameter().getPixelFormat().bpp / 8);
        int maskLen = ((w + 7) / 8) * h;
        byte[] data = new byte[dataLen];
        byte[] mask = new byte[maskLen];
        this.inputStream.readBytes(data, 0, dataLen);
        this.inputStream.readBytes(mask, 0, maskLen);
        this.handler.setCursor(hotspotX, hotspotY, w, h, data, mask);
    }
}
