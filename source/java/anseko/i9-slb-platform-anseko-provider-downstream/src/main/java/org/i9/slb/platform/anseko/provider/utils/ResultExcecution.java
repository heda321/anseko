package org.i9.slb.platform.anseko.provider.utils;

import java.io.Serializable;

/**
 * 执行命令结果
 *
 * @author R12
 * @date 2018.08.28
 */
public class ResultExcecution implements Serializable {

    private static final long serialVersionUID = 5042401086030487401L;

    /**
     * 执行命令
     */
    private String command;

    /**
     * 执行结果
     */
    private String executeResult;

    public ResultExcecution(String command, String executeResult) {
        this.command = command;
        this.executeResult = executeResult;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getExecuteResult() {
        return executeResult;
    }

    public void setExecuteResult(String executeResult) {
        this.executeResult = executeResult;
    }
}
