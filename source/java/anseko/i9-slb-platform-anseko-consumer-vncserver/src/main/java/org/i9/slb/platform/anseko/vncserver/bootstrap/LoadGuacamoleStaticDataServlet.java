package org.i9.slb.platform.anseko.vncserver.bootstrap;

import org.i9.slb.platform.anseko.vncserver.config.GuacamoleRemoteServiceProperties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 加载Guacamole配置信息常量
 *
 * @author R12
 * @date 2018.08.31
 */
public class LoadGuacamoleStaticDataServlet extends HttpServlet {

    private static final long serialVersionUID = 6820773610967681781L;

    @Override
    public void init() throws ServletException {
        super.init();
        GuacamoleRemoteServiceProperties guacamoleRemoteServiceProperties = GuacamoleRemoteServiceProperties.getInstance();
        guacamoleRemoteServiceProperties.loadProperties();
    }
}
