package org.i9.slb.platform.anseko.provider.dto;

import org.i9.slb.platform.anseko.common.types.VirtualEnum;

import java.util.List;

/**
 * 实例DTO
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:34
 */
public class InstanceListDto implements java.io.Serializable {

    private static final long serialVersionUID = -6913610814398947500L;

    private List<InstanceDto> list;

    private int totalRow;

    public List<InstanceDto> getList() {
        return list;
    }

    public void setList(List<InstanceDto> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
