package org.i9.slb.platform.anseko.provider.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟器dto
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorInfoDto implements java.io.Serializable {

    private static final long serialVersionUID = 4987711961643124407L;
    /**
     * 模拟器基础信息
     */
    private SimulatorDto simulatorDto;
    /**
     * 模拟器显示器
     */
    private SimulatorDisplayDto simulatorDisplayDto;
    /**
     * 模拟器适配器列表
     */
    private List<SimulatorAdapterDto> simulatorAdapterDtos = new ArrayList<SimulatorAdapterDto>();
    /**
     * 模拟器存储列表
     */
    private List<SimulatorStorageDto> simulatorStorageDtos = new ArrayList<SimulatorStorageDto>();
    /**
     * 模拟器端口转发列表
     */
    private List<SimulatorFirewallDto> simulatorFirewallDtos = new ArrayList<SimulatorFirewallDto>();

    public SimulatorDto getSimulatorDto() {
        return simulatorDto;
    }

    public void setSimulatorDto(SimulatorDto simulatorDto) {
        this.simulatorDto = simulatorDto;
    }

    public SimulatorDisplayDto getSimulatorDisplayDto() {
        return simulatorDisplayDto;
    }

    public void setSimulatorDisplayDto(SimulatorDisplayDto simulatorDisplayDto) {
        this.simulatorDisplayDto = simulatorDisplayDto;
    }

    public List<SimulatorAdapterDto> getSimulatorAdapterDtos() {
        return simulatorAdapterDtos;
    }

    public void setSimulatorAdapterDtos(List<SimulatorAdapterDto> simulatorAdapterDtos) {
        this.simulatorAdapterDtos = simulatorAdapterDtos;
    }

    public List<SimulatorStorageDto> getSimulatorStorageDtos() {
        return simulatorStorageDtos;
    }

    public void setSimulatorStorageDtos(List<SimulatorStorageDto> simulatorStorageDtos) {
        this.simulatorStorageDtos = simulatorStorageDtos;
    }

    public List<SimulatorFirewallDto> getSimulatorFirewallDtos() {
        return simulatorFirewallDtos;
    }

    public void setSimulatorFirewallDtos(List<SimulatorFirewallDto> simulatorFirewallDtos) {
        this.simulatorFirewallDtos = simulatorFirewallDtos;
    }
}
