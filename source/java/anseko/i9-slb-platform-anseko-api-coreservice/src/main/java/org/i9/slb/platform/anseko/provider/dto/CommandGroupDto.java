package org.i9.slb.platform.anseko.provider.dto;

import java.util.List;

/**
 * 命令调度器类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandGroupDto implements java.io.Serializable {

    private static final long serialVersionUID = -2793529293401412524L;
    /**
     * 组名称
     */
    private String name;
    /**
     * 执行组编号
     */
    private String commandGroupId;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 执行命令列表
     */
    private List<CommandExecuteDto> commandExecuteDtos;
    /**
     * 开始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
    /**
     * 是否完成
     */
    private Integer success;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CommandExecuteDto> getCommandExecuteDtos() {
        return commandExecuteDtos;
    }

    public void setCommandExecuteDtos(List<CommandExecuteDto> commandExecuteDtos) {
        this.commandExecuteDtos = commandExecuteDtos;
    }

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
