package org.i9.slb.platform.anseko.provider.dto;

/**
 * 模拟器命令组搜索
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class CommandExecuteSearchDto implements java.io.Serializable {

    private static final long serialVersionUID = -5261868017345485027L;

    private String commandGroupId;

    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
