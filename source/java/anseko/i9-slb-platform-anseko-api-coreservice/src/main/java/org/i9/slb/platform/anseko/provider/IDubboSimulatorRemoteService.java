package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorInfoDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorListDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorSearchDto;

import java.util.List;

/**
 * 模拟器远程调用服务类
 *
 * @author R12
 * @date 2018年9月4日 10:38:37
 */
public interface IDubboSimulatorRemoteService {

    /**
     * 获取所有模拟器列表
     *
     * @return
     */
    DubboResult<List<SimulatorDto>> getSimulatorDtos();

    /**
     * 获取所有模拟器列表
     *
     * @return
     */
    DubboResult<SimulatorListDto> getSimulatorDtosPage(SimulatorSearchDto simulatorSearchDto);

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    DubboResult<SimulatorDto> getSimulatorDto(String simulatorId);

    /**
     * 保存模拟器
     *
     * @param simulatorDto
     */
    DubboResult<?> createSimulatorInfo(SimulatorDto simulatorDto);

    /**
     * 删除模拟器
     *
     * @param simulatorId
     */
    DubboResult<?> removeSimulatorInfo(String simulatorId);

    /**
     * 更新模拟器状态
     *
     * @param simulatorId
     * @param powerState
     */
    DubboResult<?> refreshSimulatorPowerState(String simulatorId, int powerState);

    /**
     * 获取模拟器最大端口号
     *
     * @param instanceId
     * @return
     */
    int getNextSimulatorVNCPortNumber(String instanceId);

    /**
     * 创建模拟器
     *
     * @param simulatorInfoDto
     * @return
     */
    DubboResult<?> createSimulatorInfo0(SimulatorInfoDto simulatorInfoDto);

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    DubboResult<SimulatorInfoDto> getSimulatorInfoDto(String simulatorId);
}
