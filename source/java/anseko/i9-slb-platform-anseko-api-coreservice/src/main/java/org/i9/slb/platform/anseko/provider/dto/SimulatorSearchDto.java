package org.i9.slb.platform.anseko.provider.dto;

/**
 * 模拟器dto
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorSearchDto implements java.io.Serializable {

    private static final long serialVersionUID = 4701469880703504004L;

    private String simulatorName;

    private String instanceId;

    private Integer powerStatus;

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
