package org.i9.slb.platform.anseko.provider.dto;

/**
 * 命令回调参数
 *
 * @author jiangtao
 * @date 2019-01-16
 */
public class CommandCallbackDto implements java.io.Serializable {

    private static final long serialVersionUID = 7973607896256828761L;
    /**
     * 命令id
     */
    private String commandId;
    /**
     * 执行结果
     */
    private String executeResult;

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getExecuteResult() {
        return executeResult;
    }

    public void setExecuteResult(String executeResult) {
        this.executeResult = executeResult;
    }
}
