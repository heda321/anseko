package org.i9.slb.platform.anseko.hypervisors.handles;

import org.i9.slb.platform.anseko.hypervisors.BaseSimulatorHandle;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

/**
 * 虚拟化操作类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:48
 */
public abstract class SimulatorOperateHandle extends BaseSimulatorHandle {

    /**
     * 虚拟化操作类构造函数
     *
     * @param simulatorId
     * @param downStreamRemoteCommand
     */
    public SimulatorOperateHandle(String simulatorId, IDownStreamRemoteCommand downStreamRemoteCommand) {
        super(simulatorId, downStreamRemoteCommand);
    }

    /**
     * 启动模拟器
     *
     * @param simulatorName
     */
    public abstract void startSimulatorCommand(String simulatorName);
    /**
     * 重启模拟器
     *
     * @param simulatorName
     */
    public abstract void rebootSimulatorCommand(String simulatorName);
    /**
     * 关闭模拟器
     *
     * @param simulatorName
     */
    public abstract void shutdownSimulator(String simulatorName);
    /**
     * 取消定义模拟器命令
     *
     * @param simulatorName
     */
    public abstract void destroySimulator(String simulatorName);
}
