package org.i9.slb.platform.anseko.hypervisors;

import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

import java.util.List;

public class BaseSimulatorHandle {

    protected String simulatorId;

    protected IDownStreamRemoteCommand downStreamRemoteCommand;

    public BaseSimulatorHandle(String simulatorId, IDownStreamRemoteCommand downStreamRemoteCommand) {
        this.simulatorId = simulatorId;
        this.downStreamRemoteCommand = downStreamRemoteCommand;
    }

    /**
     * 下行指令执行shell命令操作
     *
     * @param nameLabel
     * @param shellCommandParamDto
     */
    protected void downStreamRemoteShellCommandExecute(String nameLabel, ShellCommandParamDto shellCommandParamDto) {
        this.downStreamRemoteCommand.shellCommandExecute(this.simulatorId, nameLabel, shellCommandParamDto);
    }

    /**
     * 下行指令执行shell命令操作
     *
     * @param nameLabel
     * @param shellCommandParamDtos
     */
    protected void downStreamRemoteShellCommandExecuteBatch(String nameLabel, List<ShellCommandParamDto> shellCommandParamDtos) {
        this.downStreamRemoteCommand.shellCommandExecuteBatch(this.simulatorId, nameLabel, shellCommandParamDtos);
    }
}
