package org.i9.slb.platform.anseko.hypervisors.service;

import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;

import java.util.List;

public interface IDownStreamRemoteCommand {
    /**
     * 执行shell命令操作
     *
     * @param simulatorId
     * @param nameLabel
     * @param shellCommandParamDto
     */
    void shellCommandExecute(String simulatorId, String nameLabel, ShellCommandParamDto shellCommandParamDto);

    /**
     * 执行多个shell命令操作
     *
     * @param simulatorId
     * @param nameLabel
     * @param shellCommandParamDtos
     */
    void shellCommandExecuteBatch(String simulatorId, String nameLabel, List<ShellCommandParamDto> shellCommandParamDtos);

    /**
     * 执行命令组（混合命令）
     *
     * @param simulatorId
     * @param nameLabel
     * @param groupCommandParamDto
     */
    void multipleCommandExecute(String simulatorId, String nameLabel, GroupCommandParamDto groupCommandParamDto);
}
