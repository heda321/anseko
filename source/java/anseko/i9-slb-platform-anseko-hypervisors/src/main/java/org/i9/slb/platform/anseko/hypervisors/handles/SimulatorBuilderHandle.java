package org.i9.slb.platform.anseko.hypervisors.handles;

import org.i9.slb.platform.anseko.hypervisors.BaseSimulatorHandle;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

/**
 * 虚拟化构建器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:30
 */
public abstract class SimulatorBuilderHandle extends BaseSimulatorHandle {

    protected abstract String makeDefineSimulatorFile(SimulatorInfo simulatorInfo);

    public String makeDefineSimulatorFile() {
        String content = this.makeDefineSimulatorFile(this.simulatorInfo);
        return content;
    }

    protected SimulatorInfo simulatorInfo;

    public SimulatorBuilderHandle(SimulatorInfo simulatorInfo, IDownStreamRemoteCommand downStreamRemoteCommand) {
        super(simulatorInfo.getUuid(), downStreamRemoteCommand);
        this.simulatorInfo = simulatorInfo;
    }

}
