// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
package org.i9.slb.platform.anseko.hypervisors.kvm.hypervisors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "unchecked"})
public class LibvirtVMDef {

    private String _hvsType;
    private static long _libvirtVersion;
    private static long _qemuVersion;
    private String _domName;
    private String _domUUID;
    private String _desc;
    private final Map<String, Object> components = new HashMap<String, Object>();

    public static class GuestDef {
        public enum GuestType {
            KVM, XEN, EXE, LXC;

            public GuestType convGuestType(String val) {
                for (GuestType guestType : values()) {
                    if (guestType.name().equals(val)) {
                        return guestType;
                    }
                }
                return GuestType.KVM;
            }
        }

        public enum BootOrder {

            HARDISK("hd"), CDROM("cdrom"), FLOPPY("fd"), NETWORK("network");
            String _order;

            BootOrder(String order) {
                _order = order;
            }

            @Override
            public String toString() {
                return _order;
            }

            public BootOrder convBootOrder(String val) {
                for (BootOrder bootOrder : values()) {
                    if (bootOrder.equals(val)) {
                        return bootOrder;
                    }
                }
                return BootOrder.HARDISK;
            }
        }

        private GuestType _type;
        private String _arch;
        private String _loader;
        private String _kernel;
        private String _initrd;
        private String _root;
        private String _cmdline;
        private List<BootOrder> _bootdevs = new ArrayList<BootOrder>();
        private String _machine;

        public void setGuestType(GuestType type) {
            _type = type;
        }

        public GuestType getGuestType() {
            return _type;
        }

        public void setGuestArch(String arch) {
            _arch = arch;
        }

        public void setMachineType(String machine) {
            _machine = machine;
        }

        public void setLoader(String loader) {
            _loader = loader;
        }

        public void setBootKernel(String kernel, String initrd, String rootdev,
                                  String cmdline) {
            _kernel = kernel;
            _initrd = initrd;
            _root = rootdev;
            _cmdline = cmdline;
        }

        public void setBootOrder(BootOrder order) {
            _bootdevs.add(order);
        }

        @Override
        public String toString() {
            if (_type == GuestType.KVM) {
                StringBuilder guestDef = new StringBuilder();
                guestDef.append("<os>\n");
                guestDef.append("<type");
                if (_arch != null) {
                    guestDef.append(" arch='" + _arch + "'");
                }
                if (_machine != null) {
                    guestDef.append(" machine='" + _machine + "'");
                }
                guestDef.append(">hvm</type>\n");
                if (!_bootdevs.isEmpty()) {
                    for (BootOrder bo : _bootdevs) {
                        guestDef.append("<boot dev='" + bo + "'/>\n");
                    }
                }
                guestDef.append("</os>\n");
                return guestDef.toString();
            } else if (_type == GuestType.LXC) {
                StringBuilder guestDef = new StringBuilder();
                guestDef.append("<os>\n");
                guestDef.append("<type>exe</type>\n");
                guestDef.append("<init>/sbin/init</init>\n");
                guestDef.append("</os>\n");
                return guestDef.toString();
            } else {
                return null;
            }
        }
    }

    public static class GuestResourceDef {
        private long _mem;
        private long _currentMem = -1;
        private String _memBacking;
        private int _vcpu = -1;
        private boolean _memBalloning = false;

        public void setMemorySize(long mem) {
            _mem = mem;
        }

        public void setCurrentMem(long currMem) {
            _currentMem = currMem;
        }

        public void setMemBacking(String memBacking) {
            _memBacking = memBacking;
        }

        public void setVcpuNum(int vcpu) {
            _vcpu = vcpu;
        }

        public void setMemBalloning(boolean turnon) {
            _memBalloning = turnon;
        }

        @Override
        public String toString() {
            StringBuilder resBuidler = new StringBuilder();
            resBuidler.append("<memory>" + _mem + "</memory>\n");
            if (_currentMem != -1) {
                resBuidler.append("<currentMemory>" + _currentMem
                        + "</currentMemory>\n");
            }
            if (_memBacking != null) {
                resBuidler.append("<memoryBacking>" + "<" + _memBacking + "/>"
                        + "</memoryBacking>\n");
            }
            if (_memBalloning) {
                resBuidler.append("<devices>\n" + "<memballoon model='virtio'/>\n" + "</devices>\n");
            }
            if (_vcpu != -1) {
                resBuidler.append("<vcpu>" + _vcpu + "</vcpu>\n");
            }
            return resBuidler.toString();
        }
    }

    public static class FeaturesDef {
        private final List<String> _features = new ArrayList<String>();

        public void addFeatures(String feature) {
            _features.add(feature);
        }

        @Override
        public String toString() {
            StringBuilder feaBuilder = new StringBuilder();
            feaBuilder.append("<features>\n");
            for (String feature : _features) {
                feaBuilder.append("<" + feature + "/>\n");
            }
            feaBuilder.append("</features>\n");
            return feaBuilder.toString();
        }
    }

    public static class TermPolicy {
        private String _reboot;
        private String _powerOff;
        private String _crash;

        public TermPolicy() {
            _reboot = _powerOff = _crash = "destroy";
        }

        public void setRebootPolicy(String rbPolicy) {
            _reboot = rbPolicy;
        }

        public void setPowerOffPolicy(String poPolicy) {
            _powerOff = poPolicy;
        }

        public void setCrashPolicy(String crashPolicy) {
            _crash = crashPolicy;
        }

        @Override
        public String toString() {
            StringBuilder term = new StringBuilder();
            term.append("<on_reboot>" + _reboot + "</on_reboot>\n");
            term.append("<on_poweroff>" + _powerOff + "</on_poweroff>\n");
            term.append("<on_crash>" + _powerOff + "</on_crash>\n");
            return term.toString();
        }
    }

    public static class ClockDef {
        public enum ClockOffset {
            UTC("utc"), LOCALTIME("localtime"), TIMEZONE("timezone"), VARIABLE("variable");

            private String _offset;

            private ClockOffset(String offset) {
                _offset = offset;
            }

            @Override
            public String toString() {
                return _offset;
            }

            public ClockOffset convClockOffset(String val) {
                for (ClockOffset clockOffset : values()) {
                    if (clockOffset.equals(val)) {
                        return clockOffset;
                    }
                }
                return ClockOffset.UTC;
            }
        }

        private ClockOffset _offset;
        private String _timerName;
        private String _tickPolicy;
        private String _track;

        public ClockDef() {
            _offset = ClockOffset.UTC;
        }

        public void setClockOffset(ClockOffset offset) {
            _offset = offset;
        }

        public void setTimer(String timerName, String tickPolicy, String track) {
            _timerName = timerName;
            _tickPolicy = tickPolicy;
            _track = track;
        }

        @Override
        public String toString() {
            StringBuilder clockBuilder = new StringBuilder();
            clockBuilder.append("<clock offset='");
            clockBuilder.append(_offset.toString());
            clockBuilder.append("'>\n");
            if (_timerName != null) {
                clockBuilder.append("<timer name='");
                clockBuilder.append(_timerName);
                clockBuilder.append("' ");

                if (_tickPolicy != null) {
                    clockBuilder.append("tickpolicy='");
                    clockBuilder.append(_tickPolicy);
                    clockBuilder.append("' ");
                }

                if (_track != null) {
                    clockBuilder.append("track='");
                    clockBuilder.append(_track);
                    clockBuilder.append("' ");
                }

                clockBuilder.append(">\n");
                clockBuilder.append("</timer>\n");
            }
            clockBuilder.append("</clock>\n");
            return clockBuilder.toString();
        }
    }

    public static class DevicesDef {
        private String _emulator;
        private GuestDef.GuestType _guestType;
        private final Map<String, List<?>> devices = new HashMap<String, List<?>>();

        public boolean addDevice(Object device) {
            Object dev = devices.get(device.getClass().toString());
            if (dev == null) {
                List<Object> devs = new ArrayList<Object>();
                devs.add(device);
                devices.put(device.getClass().toString(), devs);
            } else {
                List<Object> devs = (List<Object>) dev;
                devs.add(device);
            }
            return true;
        }

        public void setEmulatorPath(String emulator) {
            _emulator = emulator;
        }

        public void setGuestType(GuestDef.GuestType guestType) {
            _guestType = guestType;
        }

        @Override
        public String toString() {
            StringBuilder devicesBuilder = new StringBuilder();
            devicesBuilder.append("<devices>\n");
            if (_emulator != null) {
                devicesBuilder.append("<emulator>" + _emulator
                        + "</emulator>\n");
            }

            for (List<?> devs : devices.values()) {
                for (Object dev : devs) {
                    if (_guestType == GuestDef.GuestType.LXC) {
                        if (dev instanceof GraphicDef ||
                                dev instanceof InputDef ||
                                dev instanceof DiskDef) {
                            continue;
                        }
                    }
                    devicesBuilder.append(dev.toString());
                }
            }
            devicesBuilder.append("</devices>\n");
            return devicesBuilder.toString();
        }

        public List<DiskDef> getDisks() {
            return (List<DiskDef>) devices.get(DiskDef.class.toString());
        }

        public List<InterfaceDef> getInterfaces() {
            return (List<InterfaceDef>) devices.get(InterfaceDef.class
                    .toString());
        }

    }

    public static class DiskDef {
        public enum DeviceType {
            FLOPPY("floppy"), DISK("disk"), CDROM("cdrom");
            String _type;

            DeviceType(String type) {
                _type = type;
            }

            @Override
            public String toString() {
                return _type;
            }

            public DeviceType convDeviceType(String val) {
                for (DeviceType deviceType : values()) {
                    if (deviceType.equals(val)) {
                        return deviceType;
                    }
                }
                return DeviceType.DISK;
            }
        }

        enum DiskType {
            FILE("file"), BLOCK("block"), DIRECTROY("dir"), NETWORK("network");
            String _diskType;

            DiskType(String type) {
                _diskType = type;
            }

            @Override
            public String toString() {
                return _diskType;
            }

            public DiskType convDiskType(String val) {
                for (DiskType diskType : values()) {
                    if (diskType.equals(val)) {
                        return diskType;
                    }
                }
                return DiskType.BLOCK;
            }
        }

        public enum DiskProtocol {
            RBD("rbd"), SHEEPDOG("sheepdog"), GLUSTER("gluster");
            String _diskProtocol;

            DiskProtocol(String protocol) {
                _diskProtocol = protocol;
            }

            @Override
            public String toString() {
                return _diskProtocol;
            }

            public DiskProtocol convDiskProtocol(String val) {
                for (DiskProtocol diskProtocol : values()) {
                    if (diskProtocol.equals(val)) {
                        return diskProtocol;
                    }
                }
                return DiskProtocol.RBD;
            }
        }

        public enum DiskBus {
            IDE("ide"), SCSI("scsi"), VIRTIO("virtio"), XEN("xen"), USB("usb"), UML("uml"), FDC("fdc"), SATA("sata");
            String _bus;

            DiskBus(String bus) {
                _bus = bus;
            }

            @Override
            public String toString() {
                return _bus;
            }

            public DiskBus convDiskBus(String val) {
                for (DiskBus diskBus : values()) {
                    if (diskBus.equals(val)) {
                        return diskBus;
                    }
                }
                return DiskBus.IDE;
            }
        }

        public enum DiskFmtType {
            RAW("raw"), QCOW2("qcow2");
            String _fmtType;

            DiskFmtType(String fmt) {
                _fmtType = fmt;
            }

            @Override
            public String toString() {
                return _fmtType;
            }

            public DiskFmtType convDiskFmtType(String val) {
                for (DiskFmtType diskFmtType : values()) {
                    if (diskFmtType.equals(val)) {
                        return diskFmtType;
                    }
                }
                return DiskFmtType.RAW;
            }
        }

        private DeviceType _deviceType;
        private DiskType _diskType;
        private DiskProtocol _diskProtocol;
        private String _sourcePath;
        private String _sourceHost;
        private int _sourcePort;
        private String _authUserName;
        private String _authSecretUUID;
        private String _diskLabel;
        private DiskBus _bus;
        private DiskFmtType _diskFmtType; /* qcow2, raw etc. */
        private boolean _readonly = false;
        private boolean _shareable = false;
        private boolean _deferAttach = false;
        private Long _bytesReadRate;
        private Long _bytesWriteRate;
        private Long _iopsReadRate;
        private Long _iopsWriteRate;

        private GuestDef.GuestType _guestType;
        private int _devNum;

        public void setDeviceType(DeviceType deviceType) {
            _deviceType = deviceType;
        }

        public void defFileBasedDisk(String filePath, String diskLabel,
                                     DiskBus bus, DiskFmtType diskFmtType) {
            _diskType = DiskType.FILE;
            _deviceType = DeviceType.DISK;
            _sourcePath = filePath;
            _diskLabel = diskLabel;
            _diskFmtType = diskFmtType;
            _bus = bus;

        }

        /* skip iso label */
        private String getDevLabel(int devId, DiskBus bus) {
            /*if (devId == 2) {
                devId++;
            }*/
            char suffix = (char) ('a' + devId);
            if (bus == DiskBus.SCSI || bus == DiskBus.SATA) {
                return "sd" + suffix;
            } else if (bus == DiskBus.VIRTIO) {
                return "vd" + suffix;
            }
            return "hd" + suffix;

        }

        public void defFileBasedDisk(String filePath, int devId, DiskBus bus, DiskFmtType diskFmtType) {
            _diskType = DiskType.FILE;
            _deviceType = DeviceType.DISK;
            _sourcePath = filePath;
            _diskLabel = getDevLabel(devId, bus);
            _diskFmtType = diskFmtType;
            _bus = bus;
        }

        public void defFileBasedDisk(String filePath, int devId, DiskBus bus, DiskFmtType diskFmtType, GuestDef.GuestType guestType) {
            _diskType = DiskType.FILE;
            _deviceType = DeviceType.DISK;
            _sourcePath = filePath;
            _diskLabel = getDevLabel(devId, bus);
            _diskFmtType = diskFmtType;
            _bus = bus;
            _guestType = guestType;
            _devNum = devId;
        }

        public void defISODisk(String volPath) {
            _diskType = DiskType.FILE;
            _deviceType = DeviceType.CDROM;
            _sourcePath = volPath;
            _diskLabel = "hdc";
            _diskFmtType = DiskFmtType.RAW;
            _bus = DiskBus.IDE;
        }

        public void defISODisk0(String volPath, GuestDef.GuestType guestType) {
            _diskType = DiskType.FILE;
            _deviceType = DeviceType.DISK;
            _sourcePath = volPath;
            _diskLabel = "hdc";
            _diskFmtType = DiskFmtType.RAW;
            _bus = DiskBus.IDE;
            _guestType = guestType;
        }

        public void defBlockBasedDisk(String diskName, int devId, DiskBus bus) {
            _diskType = DiskType.BLOCK;
            _deviceType = DeviceType.DISK;
            _diskFmtType = DiskFmtType.RAW;
            _sourcePath = diskName;
            _diskLabel = getDevLabel(devId, bus);
            _bus = bus;
        }

        /**
         * 变更在这里传入磁盘主机类型(kvm, xen)
         *
         * @param diskName
         * @param devId
         * @param bus
         * @param guestType
         */
        public void defBlockBasedDisk(String diskName, int devId, DiskBus bus, GuestDef.GuestType guestType) {
            _diskType = DiskType.BLOCK;
            _deviceType = DeviceType.DISK;
            _diskFmtType = DiskFmtType.RAW;
            _sourcePath = diskName;
            _diskLabel = getDevLabel(devId, bus);
            _bus = bus;
            _guestType = guestType;
            _devNum = devId;
        }

        public void defBlockBasedDisk(String diskName, String diskLabel, DiskBus bus) {
            _diskType = DiskType.BLOCK;
            _deviceType = DeviceType.DISK;
            _diskFmtType = DiskFmtType.RAW;
            _sourcePath = diskName;
            _diskLabel = diskLabel;
            _bus = bus;
        }

        public void defNetworkBasedDisk(String diskName, String sourceHost, int sourcePort, String authUserName, String authSecretUUID, int devId, DiskBus bus, DiskProtocol protocol) {
            _diskType = DiskType.NETWORK;
            _deviceType = DeviceType.DISK;
            _diskFmtType = DiskFmtType.RAW;
            _sourcePath = diskName;
            _sourceHost = sourceHost;
            _sourcePort = sourcePort;
            _authUserName = authUserName;
            _authSecretUUID = authSecretUUID;
            _diskLabel = getDevLabel(devId, bus);
            _bus = bus;
            _diskProtocol = protocol;
        }

        public void defNetworkBasedDisk(String diskName, String sourceHost, int sourcePort, String authUserName, String authSecretUUID, String diskLabel, DiskBus bus, DiskProtocol protocol) {
            _diskType = DiskType.NETWORK;
            _deviceType = DeviceType.DISK;
            _diskFmtType = DiskFmtType.RAW;
            _sourcePath = diskName;
            _sourceHost = sourceHost;
            _sourcePort = sourcePort;
            _authUserName = authUserName;
            _authSecretUUID = authSecretUUID;
            _diskLabel = diskLabel;
            _bus = bus;
            _diskProtocol = protocol;
        }

        public void setReadonly() {
            _readonly = true;
        }

        public void setSharable() {
            _shareable = true;
        }

        public void setAttachDeferred(boolean deferAttach) {
            _deferAttach = deferAttach;
        }

        public boolean isAttachDeferred() {
            return _deferAttach;
        }

        public String getDiskPath() {
            return _sourcePath;
        }

        public String getDiskLabel() {
            return _diskLabel;
        }

        public DeviceType getDeviceType() {
            return _deviceType;
        }

        public void setDiskPath(String volPath) {
            this._sourcePath = volPath;
        }

        public DiskBus getBusType() {
            return _bus;
        }

        public int getDiskSeq() {
            char suffix = this._diskLabel.charAt(this._diskLabel.length() - 1);
            return suffix - 'a';
        }

        public void setBytesReadRate(Long bytesReadRate) {
            _bytesReadRate = bytesReadRate;
        }

        public void setBytesWriteRate(Long bytesWriteRate) {
            _bytesWriteRate = bytesWriteRate;
        }

        public void setIopsReadRate(Long iopsReadRate) {
            _iopsReadRate = iopsReadRate;
        }

        public void setIopsWriteRate(Long iopsWriteRate) {
            _iopsWriteRate = iopsWriteRate;
        }

        public String toXen() {
            StringBuilder diskBuilder = new StringBuilder();
            diskBuilder.append("<disk");
            if (_deviceType != null) {
                diskBuilder.append(" device='" + _deviceType + "'");
            }
            diskBuilder.append(" type='" + _diskType + "'");
            diskBuilder.append(">\n");
            //文件类型
            if (_diskType == DiskType.FILE) {
                diskBuilder.append("<driver name='file'" + "/>\n");
            }
            //为磁盘类型
            if (_diskType == DiskType.BLOCK) {
                diskBuilder.append("<driver name='phy'" + "/>\n");
            }
            if (_diskType == DiskType.FILE) {
                diskBuilder.append("<source ");
                if (_sourcePath != null) {
                    diskBuilder.append("file='" + _sourcePath + "'");
                } else if (_deviceType == DeviceType.CDROM) {
                    diskBuilder.append("file=''");
                }
                diskBuilder.append("/>\n");
            } else if (_diskType == DiskType.BLOCK) {
                diskBuilder.append("<source");
                if (_sourcePath != null) {
                    diskBuilder.append(" dev='" + _sourcePath + "'");
                }
                diskBuilder.append("/>\n");
            } else if (_diskType == DiskType.NETWORK) {
                diskBuilder.append("<source ");
                diskBuilder.append(" protocol='" + _diskProtocol + "'");
                diskBuilder.append(" name='" + _sourcePath + "'");
                diskBuilder.append(">\n");
                diskBuilder.append("<host name='" + _sourceHost + "' port='" + _sourcePort + "'/>\n");
                diskBuilder.append("</source>\n");
                if (_authUserName != null) {
                    diskBuilder.append("<auth username='" + _authUserName + "'>\n");
                    diskBuilder.append("<secret type='ceph' uuid='" + _authSecretUUID + "'/>\n");
                    diskBuilder.append("</auth>\n");
                }
            }
            diskBuilder.append("<target dev='" + _diskLabel + "'");
            if (_bus != null) {
                diskBuilder.append(" bus='" + _bus + "'");
            }
            diskBuilder.append("/>\n");

            if ((_deviceType != DeviceType.CDROM) && (_libvirtVersion >= 9008) && (_qemuVersion >= 1001000)
                    && (((_bytesReadRate != null) && (_bytesReadRate > 0)) || ((_bytesWriteRate != null) && (_bytesWriteRate > 0))
                    || ((_iopsReadRate != null) && (_iopsReadRate > 0)) || ((_iopsWriteRate != null) && (_iopsWriteRate > 0)))) { // not CDROM, from libvirt 0.9.8 and QEMU 1.1.0
                diskBuilder.append("<iotune>\n");
                if ((_bytesReadRate != null) && (_bytesReadRate > 0))
                    diskBuilder.append("<read_bytes_sec>" + _bytesReadRate + "</read_bytes_sec>\n");
                if ((_bytesWriteRate != null) && (_bytesWriteRate > 0))
                    diskBuilder.append("<write_bytes_sec>" + _bytesWriteRate + "</write_bytes_sec>\n");
                if ((_iopsReadRate != null) && (_iopsReadRate > 0))
                    diskBuilder.append("<read_iops_sec>" + _iopsReadRate + "</read_iops_sec>\n");
                if ((_iopsWriteRate != null) && (_iopsWriteRate > 0))
                    diskBuilder.append("<write_iops_sec>" + _iopsWriteRate + "</write_iops_sec>\n");
                diskBuilder.append("</iotune>\n");
            }

            diskBuilder.append("</disk>\n");
            return diskBuilder.toString();
        }

        public String toKvm() {
            StringBuilder diskBuilder = new StringBuilder();
            diskBuilder.append("<disk");
            if (_deviceType != null) {
                diskBuilder.append(" device='" + _deviceType + "'");
            }
            diskBuilder.append(" type='" + _diskType + "'");
            diskBuilder.append(">\n");
            diskBuilder.append("<driver name='qemu'" + " type='" + _diskFmtType + "' cache='none' " + "/>\n");
            if (_diskType == DiskType.FILE) {
                diskBuilder.append("<source ");
                if (_sourcePath != null) {
                    diskBuilder.append("file='" + _sourcePath + "'");
                } else if (_deviceType == DeviceType.CDROM) {
                    diskBuilder.append("file=''");
                }
                diskBuilder.append("/>\n");
            } else if (_diskType == DiskType.BLOCK) {
                diskBuilder.append("<source");
                if (_sourcePath != null) {
                    diskBuilder.append(" dev='" + _sourcePath + "'");
                }
                diskBuilder.append("/>\n");
            } else if (_diskType == DiskType.NETWORK) {
                diskBuilder.append("<source ");
                diskBuilder.append(" protocol='" + _diskProtocol + "'");
                diskBuilder.append(" name='" + _sourcePath + "'");
                diskBuilder.append(">\n");
                diskBuilder.append("<host name='" + _sourceHost + "' port='" + _sourcePort + "'/>\n");
                diskBuilder.append("</source>\n");
                if (_authUserName != null) {
                    diskBuilder.append("<auth username='" + _authUserName + "'>\n");
                    diskBuilder.append("<secret type='ceph' uuid='" + _authSecretUUID + "'/>\n");
                    diskBuilder.append("</auth>\n");
                }
            }
            diskBuilder.append("<target dev='" + _diskLabel + "'");
            if (_bus != null) {
                diskBuilder.append(" bus='" + _bus + "'");
            }
            diskBuilder.append("/>\n");
            if (_diskType == DiskType.BLOCK || _diskType == DiskType.FILE) {
                //TODO 修补匠 - 可以不加drive由KVM自动生成
                //diskBuilder.append(" <address type='drive' controller='0' bus='0' unit='" + _devNum + "'/>\n");
            }
            if ((_deviceType != DeviceType.CDROM) && (_libvirtVersion >= 9008) && (_qemuVersion >= 1001000)
                    && (((_bytesReadRate != null) && (_bytesReadRate > 0)) || ((_bytesWriteRate != null) && (_bytesWriteRate > 0))
                    || ((_iopsReadRate != null) && (_iopsReadRate > 0)) || ((_iopsWriteRate != null) && (_iopsWriteRate > 0)))) { // not CDROM, from libvirt 0.9.8 and QEMU 1.1.0
                diskBuilder.append("<iotune>\n");
                if ((_bytesReadRate != null) && (_bytesReadRate > 0))
                    diskBuilder.append("<read_bytes_sec>" + _bytesReadRate + "</read_bytes_sec>\n");
                if ((_bytesWriteRate != null) && (_bytesWriteRate > 0))
                    diskBuilder.append("<write_bytes_sec>" + _bytesWriteRate + "</write_bytes_sec>\n");
                if ((_iopsReadRate != null) && (_iopsReadRate > 0))
                    diskBuilder.append("<read_iops_sec>" + _iopsReadRate + "</read_iops_sec>\n");
                if ((_iopsWriteRate != null) && (_iopsWriteRate > 0))
                    diskBuilder.append("<write_iops_sec>" + _iopsWriteRate + "</write_iops_sec>\n");
                diskBuilder.append("</iotune>\n");
            }
            diskBuilder.append("</disk>\n");
            return diskBuilder.toString();
        }

        @Override
        public String toString() {
            if (_guestType == GuestDef.GuestType.KVM) {
                return toKvm();
            } else {
                return toXen();
            }
        }
    }

    public static class InterfaceDef {

        public enum GuestNetType {
            BRIDGE("bridge"), DIRECT("direct"), NETWORK("network"), USER("user"), ETHERNET("ethernet"), INTERNAL("internal");
            String _type;

            GuestNetType(String type) {
                _type = type;
            }

            @Override
            public String toString() {
                return _type;
            }

            public GuestNetType convGuestNetType(String val) {
                for (GuestNetType guestNetType : values()) {
                    if (guestNetType.equals(val)) {
                        return guestNetType;
                    }
                }
                return GuestNetType.BRIDGE;
            }
        }

        public enum NicModel {
            E1000("e1000"), VIRTIO("virtio"), RTL8139("rtl8139"), NE2KPCI("ne2k_pci");
            String _model;

            NicModel(String model) {
                _model = model;
            }

            @Override
            public String toString() {
                return _model;
            }

            public NicModel convNicModel(String val) {
                for (NicModel nicModel : values()) {
                    if (nicModel.equals(val)) {
                        return nicModel;
                    }
                }
                return NicModel.E1000;
            }
        }

        public enum HostNicType {
            DIRECT_ATTACHED_WITHOUT_DHCP, DIRECT_ATTACHED_WITH_DHCP, VNET, VLAN;

            public HostNicType convHostNicType(String val) {
                for (HostNicType hostNicType : values()) {
                    if (hostNicType.name().equals(val)) {
                        return hostNicType;
                    }
                }
                return HostNicType.DIRECT_ATTACHED_WITHOUT_DHCP;
            }
        }

        private GuestNetType _netType;
        private HostNicType _hostNetType;
        private String _netSourceMode;
        private String _sourceName;
        private String _networkName;
        private String _macAddr;
        private String _ipAddr;
        private String _scriptPath;
        private NicModel _model;
        private Integer _networkRateKBps;
        private String _virtualPortType;
        private String _virtualPortInterfaceId;
        private int _vlanTag = -1;

        public void defBridgeNet(String brName, String targetBrName,
                                 String macAddr, NicModel model) {
            defBridgeNet(brName, targetBrName, macAddr, model, 0);
        }

        public void defBridgeNet(String brName, String targetBrName,
                                 String macAddr, NicModel model, Integer networkRateKBps) {
            _netType = GuestNetType.BRIDGE;
            _sourceName = brName;
            _networkName = targetBrName;
            _macAddr = macAddr;
            _model = model;
            _networkRateKBps = networkRateKBps;
        }

        public void defDirectNet(String sourceName, String targetName, String macAddr, NicModel model, String sourceMode) {
            defDirectNet(sourceName, targetName, macAddr, model, sourceMode, 0);
        }

        public void defDirectNet(String sourceName, String targetName, String macAddr, NicModel model, String sourceMode, Integer networkRateKBps) {
            _netType = GuestNetType.DIRECT;
            _netSourceMode = sourceMode;
            _sourceName = sourceName;
            _networkName = targetName;
            _macAddr = macAddr;
            _model = model;
            _networkRateKBps = networkRateKBps;
        }

        public void defPrivateNet(String networkName, String targetName, String macAddr, NicModel model) {
            defPrivateNet(networkName, targetName, macAddr, model, 0);
        }

        public void defPrivateNet(String networkName, String targetName, String macAddr, NicModel model, Integer networkRateKBps) {
            _netType = GuestNetType.NETWORK;
            _sourceName = networkName;
            _networkName = targetName;
            _macAddr = macAddr;
            _model = model;
            _networkRateKBps = networkRateKBps;
        }

        public void defEthernet(String targetName, String macAddr, NicModel model, String scriptPath) {
            defEthernet(targetName, macAddr, model, scriptPath, 0);
        }

        public void defEthernet(String targetName, String macAddr, NicModel model, String scriptPath, Integer networkRateKBps) {
            _netType = GuestNetType.ETHERNET;
            _networkName = targetName;
            _sourceName = targetName;
            _macAddr = macAddr;
            _model = model;
            _scriptPath = scriptPath;
            _networkRateKBps = networkRateKBps;
        }

        public void defEthernet(String targetName, String macAddr, NicModel model) {
            defEthernet(targetName, macAddr, model, null);
        }

        public void setHostNetType(HostNicType hostNetType) {
            _hostNetType = hostNetType;
        }

        public HostNicType getHostNetType() {
            return _hostNetType;
        }

        public String getBrName() {
            return _sourceName;
        }

        public GuestNetType getNetType() {
            return _netType;
        }

        public String getNetSourceMode() {
            return _netSourceMode;
        }

        public String getDevName() {
            return _networkName;
        }

        public String getMacAddress() {
            return _macAddr;
        }

        public void setVirtualPortType(String virtualPortType) {
            _virtualPortType = virtualPortType;
        }

        public String getVirtualPortType() {
            return _virtualPortType;
        }

        public void setVirtualPortInterfaceId(String virtualPortInterfaceId) {
            _virtualPortInterfaceId = virtualPortInterfaceId;
        }

        public String getVirtualPortInterfaceId() {
            return _virtualPortInterfaceId;
        }

        public void setVlanTag(int vlanTag) {
            _vlanTag = vlanTag;
        }

        public int getVlanTag() {
            return _vlanTag;
        }

        @Override
        public String toString() {
            StringBuilder netBuilder = new StringBuilder();
            netBuilder.append("<interface type='" + _netType + "'>\n");
            if (_netType == GuestNetType.BRIDGE) {
                netBuilder.append("<source bridge='" + _sourceName + "'/>\n");
            } else if (_netType == GuestNetType.NETWORK) {
                netBuilder.append("<source network='" + _sourceName + "'/>\n");
            } else if (_netType == GuestNetType.DIRECT) {
                netBuilder.append("<source dev='" + _sourceName + "' mode='" + _netSourceMode + "'/>\n");
            }
            if (_networkName != null) {
                netBuilder.append("<target dev='" + _networkName + "'/>\n");
            }
            if (_macAddr != null) {
                netBuilder.append("<mac address='" + _macAddr + "'/>\n");
            }
            if (_model != null) {
                netBuilder.append("<model type='" + _model + "'/>\n");
            }
            if (_networkRateKBps > 0) {
                _networkRateKBps = _networkRateKBps * 1024 / 8;
                netBuilder.append("<bandwidth>\n");
                netBuilder.append("<inbound average='" + _networkRateKBps + "' peak='" + _networkRateKBps + "'/>\n");
                netBuilder.append("<outbound average='" + _networkRateKBps + "' peak='" + _networkRateKBps + "'/>\n");
                netBuilder.append("</bandwidth>\n");
            }
            if (_scriptPath != null) {
                netBuilder.append("<script path='" + _scriptPath + "'/>\n");
            }
            if (_virtualPortType != null) {
                netBuilder.append("<virtualport type='" + _virtualPortType + "'>\n");
                if (_virtualPortInterfaceId != null) {
                    netBuilder.append("<parameters interfaceid='" + _virtualPortInterfaceId + "'/>\n");
                }
                netBuilder.append("</virtualport>\n");
            }
            if (_vlanTag > 0 && _vlanTag < 4095) {
                netBuilder.append("<vlan trunk='no'>\n<tag id='" + _vlanTag + "'/>\n</vlan>");
            }
            netBuilder.append("</interface>\n");
            return netBuilder.toString();
        }
    }

    public static class ConsoleDef {
        private final String _ttyPath;
        private final String _type;
        private final String _source;
        private short _port = -1;

        public ConsoleDef(String type, String path, String source, short port) {
            _type = type;
            _ttyPath = path;
            _source = source;
            _port = port;
        }

        @Override
        public String toString() {
            StringBuilder consoleBuilder = new StringBuilder();
            consoleBuilder.append("<console ");
            consoleBuilder.append("type='" + _type + "'");
            if (_ttyPath != null) {
                consoleBuilder.append("tty='" + _ttyPath + "'");
            }
            consoleBuilder.append(">\n");
            if (_source != null) {
                consoleBuilder.append("<source path='" + _source + "'/>\n");
            }
            if (_port != -1) {
                consoleBuilder.append("<target port='" + _port + "'/>\n");
            }
            consoleBuilder.append("</console>\n");
            return consoleBuilder.toString();
        }
    }

    public static class CpuTuneDef {
        private int _shares = 0;

        public void setShares(int shares) {
            _shares = shares;
        }

        public int getShares() {
            return _shares;
        }

        @Override
        public String toString() {
            StringBuilder cpuTuneBuilder = new StringBuilder();
            cpuTuneBuilder.append("<cputune>\n");
            if (_shares > 0) {
                cpuTuneBuilder.append("<shares>" + _shares + "</shares>\n");
            }
            cpuTuneBuilder.append("</cputune>\n");
            return cpuTuneBuilder.toString();
        }
    }

    public static class CpuModeDef {
        private String _mode;
        private String _model;

        public void setMode(String mode) {
            _mode = mode;
        }

        public void setModel(String model) {
            _model = model;
        }

        @Override
        public String toString() {
            StringBuilder modeBuidler = new StringBuilder();
            if ("custom".equalsIgnoreCase(_mode) && _model != null) {
                modeBuidler.append("<cpu mode='custom' match='exact'><model fallback='allow'>" + _model + "</model></cpu>");
            } else if ("host-model".equals(_mode)) {
                modeBuidler.append("<cpu mode='host-model'><model fallback='allow'></model></cpu>");
            } else if ("host-passthrough".equals(_mode)) {
                modeBuidler.append("<cpu mode='host-passthrough'></cpu>");
            }
            return modeBuidler.toString();
        }
    }

    public static class SerialDef {
        private final String _type;
        private final String _source;
        private short _port = -1;

        public SerialDef(String type, String source, short port) {
            _type = type;
            _source = source;
            _port = port;
        }

        @Override
        public String toString() {
            StringBuilder serialBuidler = new StringBuilder();
            serialBuidler.append("<serial type='" + _type + "'>\n");
            if (_source != null) {
                serialBuidler.append("<source path='" + _source + "'/>\n");
            }
            if (_port != -1) {
                serialBuidler.append("<target port='" + _port + "'/>\n");
            }
            serialBuidler.append("</serial>\n");
            return serialBuidler.toString();
        }
    }

    public static class VirtioSerialDef {
        private final String _name;
        private String _path;

        public VirtioSerialDef(String name, String path) {
            _name = name;
            _path = path;
        }

        @Override
        public String toString() {
            StringBuilder virtioSerialBuilder = new StringBuilder();
            if (_path == null) {
                _path = "/var/lib/libvirt/qemu";
            }
            virtioSerialBuilder.append("<channel type='unix'>\n");
            virtioSerialBuilder.append("<source mode='bind' path='" + _path + "/" + _name + ".agent'/>\n");
            virtioSerialBuilder.append("<target type='virtio' name='" + _name + ".vport'/>\n");
            virtioSerialBuilder.append("<address type='virtio-serial'/>\n");
            virtioSerialBuilder.append("</channel>\n");
            return virtioSerialBuilder.toString();
        }
    }

    public static class GraphicDef {
        private final String _type;
        private short _port = -2;
        private boolean _autoPort = false;
        private final String _listenAddr;
        private final String _passwd;
        private final String _keyMap;

        public GraphicDef(String type, short port, boolean autoPort,
                          String listenAddr, String passwd, String keyMap) {
            _type = type;
            _port = port;
            _autoPort = autoPort;
            _listenAddr = listenAddr;
            _passwd = passwd;
            _keyMap = keyMap;
        }

        @Override
        public String toString() {
            StringBuilder graphicBuilder = new StringBuilder();
            graphicBuilder.append("<graphics type='" + _type + "'");
            if (_autoPort) {
                graphicBuilder.append(" autoport='yes'");
            } else if (_port != -2) {
                graphicBuilder.append(" port='" + _port + "'");
            }
            if (_listenAddr != null) {
                graphicBuilder.append(" listen='" + _listenAddr + "'");
            } else {
                graphicBuilder.append(" listen=''");
            }
            if (_passwd != null) {
                graphicBuilder.append(" passwd='" + _passwd + "'");
            } else if (_keyMap != null) {
                graphicBuilder.append(" _keymap='" + _keyMap + "'");
            }
            graphicBuilder.append("/>\n");
            return graphicBuilder.toString();
        }
    }

    public static class InputDef {
        private final String _type;
        private final String _bus;

        public InputDef(String type, String bus) {
            _type = type;
            _bus = bus;
        }

        @Override
        public String toString() {
            StringBuilder inputBuilder = new StringBuilder();
            inputBuilder.append("<input type='" + _type + "'");
            if (_bus != null) {
                inputBuilder.append(" bus='" + _bus + "'");
            }
            inputBuilder.append("/>\n");
            return inputBuilder.toString();
        }
    }

    public static class VideoDef {
        private final String _type;
        private final int _vram;
        private final int _heads;

        public VideoDef(String type, int vram, int heads) {
            _type = type;
            _vram = vram;
            _heads = heads;
        }

        public VideoDef(String type) {
            this(type, 9216, 1);
        }

        @Override
        public String toString() {
            StringBuilder videoBuilder = new StringBuilder();
            videoBuilder.append("<video>\n");
            videoBuilder.append("<model type='" + _type + "' vram='" + _vram + "' heads='" + _heads + "'/>\n");
            videoBuilder.append("</video>\n");
            return videoBuilder.toString();
        }
    }

    public static class FilesystemDef {
        private final String _sourcePath;
        private final String _targetPath;

        public FilesystemDef(String sourcePath, String targetPath) {
            _sourcePath = sourcePath;
            _targetPath = targetPath;
        }

        @Override
        public String toString() {
            StringBuilder fsBuilder = new StringBuilder();
            fsBuilder.append("<filesystem type='mount'>\n");
            fsBuilder.append("  <source dir='" + _sourcePath + "'/>\n");
            fsBuilder.append("  <target dir='" + _targetPath + "'/>\n");
            fsBuilder.append("</filesystem>\n");
            return fsBuilder.toString();
        }
    }

    public void setHvsType(String hvs) {
        _hvsType = hvs;
    }

    public String getHvsType() {
        return _hvsType;
    }

    public void setLibvirtVersion(long libvirtVersion) {
        _libvirtVersion = libvirtVersion;
    }

    public void setQemuVersion(long qemuVersion) {
        _qemuVersion = qemuVersion;
    }

    public void setDomainName(String domainName) {
        _domName = domainName;
    }

    public void setDomUUID(String uuid) {
        _domUUID = uuid;
    }

    public void setDomDescription(String desc) {
        _desc = desc;
    }

    public String getGuestOSType() {
        return _desc;
    }

    public void addComp(Object comp) {
        components.put(comp.getClass().toString(), comp);
    }

    public DevicesDef getDevices() {
        Object o = components.get(DevicesDef.class.toString());
        if (o != null) {
            return (DevicesDef) o;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder vmBuilder = new StringBuilder();
        vmBuilder.append("<domain type='" + _hvsType + "'>\n");
        vmBuilder.append("<name>" + _domName + "</name>\n");
        if (_domUUID != null) {
            vmBuilder.append("<uuid>" + _domUUID + "</uuid>\n");
        }
        if (_desc != null) {
            vmBuilder.append("<description>" + _desc + "</description>\n");
        }
        for (Object o : components.values()) {
            vmBuilder.append(o.toString());
        }
        vmBuilder.append("</domain>\n");
        return vmBuilder.toString();
    }
}
