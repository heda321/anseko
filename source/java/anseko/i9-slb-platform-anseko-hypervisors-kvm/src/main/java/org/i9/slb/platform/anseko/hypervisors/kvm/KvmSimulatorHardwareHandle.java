package org.i9.slb.platform.anseko.hypervisors.kvm;

import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorHardwareHandle;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorDisk;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorNetwork;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;
import org.i9.slb.platform.anseko.hypervisors.utils.SimulatorUtils;

import java.util.UUID;

/**
 * kvm 虚拟化硬件操作类
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/4/19 15:47
 */
public class KvmSimulatorHardwareHandle extends SimulatorHardwareHandle {

    private String simulatorName;

    /**
     * 虚拟化操作类构造函数
     *
     * @param simulatorId
     * @param downStreamRemoteCommand
     */
    public KvmSimulatorHardwareHandle(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    /**
     * 创建物理磁盘
     *
     * @return
     */
    @Override
    public String[] createDiskPath() {
        String[] commands = new String[2 * 1];
        SimulatorDisk simulatorDisk = this.buildSimulatorDisks()[0];
        commands[0] = "lvcreate -L " + simulatorDisk.getDiskSize() + "G -n " + this.simulatorName + " disk1";
        commands[1] = "dd if=/dev/disk1/android.template.idc9000.com of=" + simulatorDisk.getDiskPath() + " bs=1G count=1000";
        return commands;
    }

    /**
     * 获取模拟器磁盘
     *
     * @return
     */
    @Override
    public SimulatorDisk[] buildSimulatorDisks() {
        SimulatorDisk[] simulatorDisks = new SimulatorDisk[1];
        simulatorDisks[0] = new SimulatorDisk(UUID.randomUUID().toString(), "/dev/disk1/" + this.simulatorName, 3);
        return simulatorDisks;
    }

    /**
     * 获取模拟器网络
     *
     * @return
     */
    @Override
    public SimulatorNetwork[] buildSimulatorNetworks() {
        SimulatorNetwork[] simulatorNetworks = new SimulatorNetwork[1];
        simulatorNetworks[0] = new SimulatorNetwork(
                UUID.randomUUID().toString(), "vnet-" + this.simulatorName + "-1", SimulatorUtils.randomMAC(), 10);
        return simulatorNetworks;
    }
}
