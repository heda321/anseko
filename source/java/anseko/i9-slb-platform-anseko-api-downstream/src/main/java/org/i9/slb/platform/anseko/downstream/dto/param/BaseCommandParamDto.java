package org.i9.slb.platform.anseko.downstream.dto.param;

/**
 * 基础命令参数
 *
 * @author r12
 * @version 1.0
 * @date 2019/4/19 15:04
 */
public class BaseCommandParamDto implements java.io.Serializable {

    private static final long serialVersionUID = 1571761121652819773L;
}
